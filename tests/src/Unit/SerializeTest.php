<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\DateTime\Unit;

use BjoernGoetschke\DateTime\Date;
use BjoernGoetschke\DateTime\Interval;
use BjoernGoetschke\DateTime\LocalMoment;
use BjoernGoetschke\DateTime\Moment;
use PHPUnit\Framework\TestCase;

final class SerializeTest extends TestCase
{
    public function testSerializePositiveMoment(): void
    {
        $original = Moment::fromStringNotNull('2022-10-02T03:04:05.678901+02:00');

        $recreated = unserialize(serialize($original));
        self::assertInstanceOf(Moment::class, $recreated);

        self::assertNotSame($original, $recreated);
        self::assertEquals($original, $recreated);
    }

    public function testSerializeNegativeMoment(): void
    {
        $original = Moment::fromStringNotNull('-2022-10-02T03:04:05.678901+02:00');

        $recreated = unserialize(serialize($original));
        self::assertInstanceOf(Moment::class, $recreated);

        self::assertNotSame($original, $recreated);
        self::assertEquals($original, $recreated);
    }

    public function testSerializePositiveLocalMoment(): void
    {
        $original = LocalMoment::fromStringNotNull('2022-10-02T03:04:05.678901+02:00');

        $recreated = unserialize(serialize($original));
        self::assertInstanceOf(LocalMoment::class, $recreated);

        self::assertNotSame($original, $recreated);
        self::assertEquals($original, $recreated);
    }

    public function testSerializeNegativeLocalMoment(): void
    {
        $original = LocalMoment::fromStringNotNull('-2022-10-02T03:04:05.678901+02:00');

        $recreated = unserialize(serialize($original));
        self::assertInstanceOf(LocalMoment::class, $recreated);

        self::assertNotSame($original, $recreated);
        self::assertEquals($original, $recreated);
    }

    public function testSerializePositiveDate(): void
    {
        $original = Date::fromStringNotNull('2022-10-02T03:04:05.678901+02:00');

        $recreated = unserialize(serialize($original));
        self::assertInstanceOf(Date::class, $recreated);

        self::assertNotSame($original, $recreated);
        self::assertEquals($original, $recreated);
    }

    public function testSerializeNegativeDate(): void
    {
        $original = Date::fromStringNotNull('-2022-10-02T03:04:05.678901+02:00');

        $recreated = unserialize(serialize($original));
        self::assertInstanceOf(Date::class, $recreated);

        self::assertNotSame($original, $recreated);
        self::assertEquals($original, $recreated);
    }

    public function testSerializePositiveInterval(): void
    {
        $original = Interval::fromString('P2Y5M11DT7H54M33S012345F');

        $recreated = unserialize(serialize($original));
        self::assertInstanceOf(Interval::class, $recreated);

        self::assertNotSame($original, $recreated);
        self::assertEquals($original, $recreated);
    }

    public function testSerializeNegativeInterval(): void
    {
        $original = Interval::fromString('P2Y5M11DT7H54M33S012345F-');

        $recreated = unserialize(serialize($original));
        self::assertInstanceOf(Interval::class, $recreated);

        self::assertNotSame($original, $recreated);
        self::assertEquals($original, $recreated);
    }
}
