<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\DateTime\Unit;

use BjoernGoetschke\DateTime\Date;
use BjoernGoetschke\DateTime\Moment;
use DateTime;
use DateTimeImmutable;
use DateTimeZone;
use PHPUnit\Framework\TestCase;

final class DateUtilityConstructorsTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Moment::resetNow();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Moment::resetNow();
    }

    public function testToday(): void
    {
        Moment::setNow(Moment::fromStringNotNull('2018-01-01T00:00:00+00:00'));

        self::assertSame(
            '2017-12-31',
            (string)Date::today(new DateTimeZone('-05:00')),
        );

        self::assertSame(
            '2018-01-01',
            (string)Date::today(new DateTimeZone('+00:00')),
        );

        self::assertSame(
            '2018-01-01',
            (string)Date::today(new DateTimeZone('+01:00')),
        );
    }

    public function testFromStringNullInputReturnsNull(): void
    {
        $date = Date::fromString(null);

        self::assertNull(
            $date,
        );
    }

    public function testFromStringDateOnly(): void
    {
        $date = Date::fromString('2020-10-31');

        self::assertNotNull(
            $date,
        );

        self::assertSame(
            '2020-10-31',
            (string)$date,
        );

        self::assertSame(
            '2020-10-31T00:00:00.000000+00:00',
            (string)$date->toMoment(),
        );
    }

    public function testFromStringFullDateTimeAndDifferentDateInUTC(): void
    {
        $date = Date::fromString('2020-10-31T23:24:25.123456-08:00');

        self::assertNotNull(
            $date,
        );

        self::assertSame(
            '2020-10-31',
            (string)$date,
        );

        self::assertSame(
            '2020-10-31T00:00:00.000000+00:00',
            (string)$date->toMoment(),
        );
    }

    public function testFromDateTimeNullInputReturnsNull(): void
    {
        $date = Date::fromDateTime(null);

        self::assertNull(
            $date,
        );
    }

    public function testFromDateTimeDateOnly(): void
    {
        $date1 = Date::fromDateTime(new DateTimeImmutable('2020-10-31'));

        self::assertNotNull(
            $date1,
        );

        self::assertSame(
            '2020-10-31',
            (string)$date1,
        );

        self::assertSame(
            '2020-10-31T00:00:00.000000+00:00',
            (string)$date1->toMoment(),
        );

        $date2 = Date::fromDateTime(new DateTime('2020-10-31'));

        self::assertNotNull(
            $date2,
        );

        self::assertSame(
            '2020-10-31',
            (string)$date2,
        );

        self::assertSame(
            '2020-10-31T00:00:00.000000+00:00',
            (string)$date2->toMoment(),
        );
    }

    public function testFromDateTimeFullDateTimeAndDifferentDateInUTC(): void
    {
        $date1 = Date::fromDateTime(new DateTimeImmutable('2020-10-31T23:24:25.123456-08:00'));

        self::assertNotNull(
            $date1,
        );

        self::assertSame(
            '2020-10-31',
            (string)$date1,
        );

        self::assertSame(
            '2020-10-31T00:00:00.000000+00:00',
            (string)$date1->toMoment(),
        );

        $date2 = Date::fromDateTime(new DateTime('2020-10-31T23:24:25.123456-08:00'));

        self::assertNotNull(
            $date2,
        );

        self::assertSame(
            '2020-10-31',
            (string)$date2,
        );

        self::assertSame(
            '2020-10-31T00:00:00.000000+00:00',
            (string)$date2->toMoment(),
        );
    }
}
