<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\DateTime\Unit;

use BjoernGoetschke\DateTime\Interval;
use BjoernGoetschke\DateTime\LocalMoment;
use BjoernGoetschke\DateTime\Moment;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class LocalMomentTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Moment::resetNow();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Moment::resetNow();
    }

    public function testInvalidFormatThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);

        LocalMoment::fromString('SomeInvalidFormat');
    }

    public function testFormatDefault(): void
    {
        $moment = LocalMoment::fromStringNotNull('2016-09-19T03:04:05.123456');

        self::assertSame(
            '2016-09-19T03:04:05.123456',
            (string)$moment,
        );
    }

    public function testToString(): void
    {
        $moment = LocalMoment::fromStringNotNull('2016-09-19T03:04:05.123456');

        self::assertSame(
            '2016-09-19T03:04:05.123456',
            (string)$moment,
        );
    }

    public function testToMoment(): void
    {
        $localMoment = LocalMoment::fromStringNotNull('2017-07-08T09:10:11.123456');
        $moment = $localMoment->toMoment();

        self::assertSame(
            '2017-07-08T09:10:11.123456+00:00',
            (string)$moment,
        );
    }

    public function testToDate(): void
    {
        $localMoment = LocalMoment::fromStringNotNull('2017-06-07T02:03:04.123456');
        $date = $localMoment->toDate();

        self::assertSame(
            '2017-06-07',
            (string)$date,
        );
    }

    public function testDiff(): void
    {
        $localMoment1 = LocalMoment::fromStringNotNull('2017-02-03T04:05:06.123456');
        $localMoment2 = LocalMoment::fromStringNotNull('2017-02-04T06:08:10.654321');
        $diff1 = $localMoment1->diff($localMoment2);

        self::assertSame(
            'P0Y0M1DT2H3M4S530865F+',
            (string)$diff1,
        );

        $diff2 = $localMoment2->diff($localMoment1);

        self::assertSame(
            'P0Y0M1DT2H3M4S530865F-',
            (string)$diff2,
        );
    }

    public function testAdd(): void
    {
        $localMoment1 = LocalMoment::fromStringNotNull('2017-03-04T05:06:07.123456');
        $localMoment2 = $localMoment1->add(Interval::fromString('P1M2DT5H4M3S'));

        self::assertNotSame(
            $localMoment1,
            $localMoment2,
        );

        self::assertSame(
            '2017-04-06T10:10:10.123456',
            (string)$localMoment2,
        );
    }

    public function testSub(): void
    {
        $localMoment1 = LocalMoment::fromStringNotNull('2017-03-04T05:06:07.123456');
        $localMoment2 = $localMoment1->sub(Interval::fromString('P2M3DT5H6M7S'));

        self::assertNotSame(
            $localMoment1,
            $localMoment2,
        );

        self::assertSame(
            '2017-01-01T00:00:00.123456',
            (string)$localMoment2,
        );
    }

    public function testIsAfter(): void
    {
        $localMoment1 = LocalMoment::fromStringNotNull('2017-03-04T05:06:07');
        $localMoment2 = LocalMoment::fromStringNotNull('2017-03-04T05:06:08');
        $localMoment3 = LocalMoment::fromStringNotNull('2017-03-04T05:06:09');

        self::assertFalse(
            $localMoment1->isAfter($localMoment2),
        );

        self::assertFalse(
            $localMoment1->isAfter($localMoment3),
        );

        self::assertTrue(
            $localMoment2->isAfter($localMoment1),
        );

        self::assertFalse(
            $localMoment2->isAfter($localMoment3),
        );

        self::assertTrue(
            $localMoment3->isAfter($localMoment1),
        );

        self::assertTrue(
            $localMoment3->isAfter($localMoment2),
        );
    }

    public function testIsAfterOrEqual(): void
    {
        $localMoment1 = LocalMoment::fromStringNotNull('2017-03-04T05:06:07');
        $localMoment2 = LocalMoment::fromStringNotNull('2017-03-04T05:06:08');
        $localMoment3 = LocalMoment::fromStringNotNull('2017-03-04T05:06:08');
        $localMoment4 = LocalMoment::fromStringNotNull('2017-03-04T05:06:09');

        self::assertFalse(
            $localMoment1->isAfterOrEqual($localMoment2),
        );

        self::assertFalse(
            $localMoment1->isAfterOrEqual($localMoment3),
        );

        self::assertFalse(
            $localMoment1->isAfterOrEqual($localMoment4),
        );

        self::assertTrue(
            $localMoment2->isAfterOrEqual($localMoment1),
        );

        self::assertTrue(
            $localMoment2->isAfterOrEqual($localMoment3),
        );

        self::assertFalse(
            $localMoment2->isAfterOrEqual($localMoment4),
        );

        self::assertTrue(
            $localMoment3->isAfterOrEqual($localMoment1),
        );

        self::assertTrue(
            $localMoment3->isAfterOrEqual($localMoment2),
        );

        self::assertFalse(
            $localMoment3->isAfterOrEqual($localMoment4),
        );

        self::assertTrue(
            $localMoment4->isAfterOrEqual($localMoment1),
        );

        self::assertTrue(
            $localMoment4->isAfterOrEqual($localMoment2),
        );

        self::assertTrue(
            $localMoment4->isAfterOrEqual($localMoment3),
        );
    }

    public function testIsBefore(): void
    {
        $localMoment1 = LocalMoment::fromStringNotNull('2017-03-04T05:06:07');
        $localMoment2 = LocalMoment::fromStringNotNull('2017-03-04T05:06:08');
        $localMoment3 = LocalMoment::fromStringNotNull('2017-03-04T05:06:09');

        self::assertTrue(
            $localMoment1->isBefore($localMoment2),
        );

        self::assertTrue(
            $localMoment1->isBefore($localMoment3),
        );

        self::assertFalse(
            $localMoment2->isBefore($localMoment1),
        );

        self::assertTrue(
            $localMoment2->isBefore($localMoment3),
        );

        self::assertFalse(
            $localMoment3->isBefore($localMoment1),
        );

        self::assertFalse(
            $localMoment3->isBefore($localMoment2),
        );
    }

    public function testIsBeforeOrEqual(): void
    {
        $localMoment1 = LocalMoment::fromStringNotNull('2017-03-04T05:06:07');
        $localMoment2 = LocalMoment::fromStringNotNull('2017-03-04T05:06:08');
        $localMoment3 = LocalMoment::fromStringNotNull('2017-03-04T05:06:08');
        $localMoment4 = LocalMoment::fromStringNotNull('2017-03-04T05:06:09');

        self::assertTrue(
            $localMoment1->isBeforeOrEqual($localMoment2),
        );

        self::assertTrue(
            $localMoment1->isBeforeOrEqual($localMoment3),
        );

        self::assertTrue(
            $localMoment1->isBeforeOrEqual($localMoment4),
        );

        self::assertFalse(
            $localMoment2->isBeforeOrEqual($localMoment1),
        );

        self::assertTrue(
            $localMoment2->isBeforeOrEqual($localMoment3),
        );

        self::assertTrue(
            $localMoment2->isBeforeOrEqual($localMoment4),
        );

        self::assertFalse(
            $localMoment3->isBeforeOrEqual($localMoment1),
        );

        self::assertTrue(
            $localMoment3->isBeforeOrEqual($localMoment2),
        );

        self::assertTrue(
            $localMoment3->isBeforeOrEqual($localMoment4),
        );

        self::assertFalse(
            $localMoment4->isBeforeOrEqual($localMoment1),
        );

        self::assertFalse(
            $localMoment4->isBeforeOrEqual($localMoment2),
        );

        self::assertFalse(
            $localMoment4->isBeforeOrEqual($localMoment3),
        );
    }

    public function testEquals(): void
    {
        $localMoment1 = LocalMoment::fromStringNotNull('2017-03-04T05:06:07');
        $localMoment2 = LocalMoment::fromStringNotNull('2017-03-04T05:06:08');
        $localMoment3 = LocalMoment::fromStringNotNull('2017-03-04T05:06:08');
        $localMoment4 = LocalMoment::fromStringNotNull('2017-03-04T05:06:09');

        self::assertFalse(
            $localMoment1->equals($localMoment2),
        );

        self::assertFalse(
            $localMoment1->equals($localMoment3),
        );

        self::assertFalse(
            $localMoment1->equals($localMoment4),
        );

        self::assertFalse(
            $localMoment2->equals($localMoment1),
        );

        self::assertTrue(
            $localMoment2->equals($localMoment3),
        );

        self::assertFalse(
            $localMoment2->equals($localMoment4),
        );

        self::assertFalse(
            $localMoment3->equals($localMoment1),
        );

        self::assertTrue(
            $localMoment3->equals($localMoment2),
        );

        self::assertFalse(
            $localMoment3->equals($localMoment4),
        );

        self::assertFalse(
            $localMoment4->equals($localMoment1),
        );

        self::assertFalse(
            $localMoment4->equals($localMoment2),
        );

        self::assertFalse(
            $localMoment4->equals($localMoment3),
        );
    }
}
