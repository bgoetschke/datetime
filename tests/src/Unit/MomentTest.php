<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\DateTime\Unit;

use BjoernGoetschke\DateTime\Interval;
use BjoernGoetschke\DateTime\Moment;
use DateTimeZone;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class MomentTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Moment::resetNow();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Moment::resetNow();
    }

    public function testSettingFixedMomentResultsInHasNowReturningTrue(): void
    {
        self::assertFalse(Moment::hasNow());

        Moment::setNow(Moment::fromStringNotNull('2022-10-01T01:02:03+04:00'));

        self::assertTrue(Moment::hasNow());

        Moment::resetNow();

        self::assertFalse(Moment::hasNow());
    }

    public function testWithUtcTimezone(): void
    {
        $localMoment = Moment::fromStringNotNull('2017-04-03T06:08:10+01:00');
        $utcMoment = $localMoment->withUtcTimezone();

        self::assertNotSame(
            $localMoment,
            $utcMoment,
        );

        self::assertTrue(
            $localMoment->equals($utcMoment),
        );

        self::assertSame(
            $localMoment->getTimestamp(),
            $utcMoment->getTimestamp(),
        );

        self::assertSame(
            '2017-04-03T06:08:10.000000+01:00',
            (string)$localMoment,
        );

        self::assertSame(
            '2017-04-03T05:08:10.000000+00:00',
            (string)$utcMoment,
        );
    }

    public function testDiscardTimezone(): void
    {
        $localMoment = Moment::fromStringNotNull('2020-06-29T12:00:00.123456+06:00');
        $utcMoment = $localMoment->discardTimezone();

        self::assertNotSame(
            $localMoment,
            $utcMoment,
        );

        self::assertFalse(
            $localMoment->equals($utcMoment),
        );

        self::assertNotSame(
            $localMoment->getTimestamp(),
            $utcMoment->getTimestamp(),
        );

        self::assertSame(
            '2020-06-29T12:00:00.123456+06:00',
            (string)$localMoment,
        );

        self::assertSame(
            '2020-06-29T12:00:00.123456+00:00',
            (string)$utcMoment,
        );
    }

    public function testReplaceTimezoneWithPositiveDateTime(): void
    {
        $originalMoment = Moment::fromStringNotNull('2020-06-29T12:00:00.123456+06:00');
        $newMoment = $originalMoment->replaceTimezone(new DateTimeZone('+02:00'));

        self::assertNotSame(
            $originalMoment,
            $newMoment,
        );

        self::assertFalse(
            $originalMoment->equals($newMoment),
        );

        self::assertNotSame(
            $originalMoment->getTimestamp(),
            $newMoment->getTimestamp(),
        );

        self::assertSame(
            '2020-06-29T12:00:00.123456+06:00',
            (string)$originalMoment,
        );

        self::assertSame(
            '2020-06-29T12:00:00.123456+02:00',
            (string)$newMoment,
        );
    }

    public function testReplaceTimezoneWithNegativeDateTime(): void
    {
        $originalMoment = Moment::fromStringNotNull('-2020-06-29T12:00:00.123456+06:00');
        $newMoment = $originalMoment->replaceTimezone(new DateTimeZone('+02:00'));

        self::assertNotSame(
            $originalMoment,
            $newMoment,
        );

        self::assertFalse(
            $originalMoment->equals($newMoment),
        );

        self::assertNotSame(
            $originalMoment->getTimestamp(),
            $newMoment->getTimestamp(),
        );

        self::assertSame(
            '-2020-06-29T12:00:00.123456+06:00',
            (string)$originalMoment,
        );

        self::assertSame(
            '-2020-06-29T12:00:00.123456+02:00',
            (string)$newMoment,
        );
    }

    public function testToString(): void
    {
        $moment = Moment::fromStringNotNull('2017-04-05T06:07:08.123456+03:00');

        self::assertSame(
            '2017-04-05T06:07:08.123456+03:00',
            (string)$moment,
        );
    }

    public function testToLocalMoment(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-06-07T08:09:10.123456+00:00');
        $localMoment1 = $moment1->toLocalMoment();

        self::assertSame(
            '2017-06-07T08:09:10.123456',
            (string)$localMoment1,
        );

        $moment2 = Moment::fromStringNotNull('2017-06-07T08:09:10.123456+04:00');
        $localMoment2 = $moment2->toLocalMoment();

        self::assertSame(
            '2017-06-07T08:09:10.123456',
            (string)$localMoment2,
        );
    }

    public function testToDate(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-06-07T02:03:04+00:00');
        $date1 = $moment1->toDate();

        self::assertSame(
            '2017-06-07',
            (string)$date1,
        );

        $moment2 = Moment::fromStringNotNull('2017-06-07T02:03:04+04:00');
        $date2 = $moment2->toDate();

        self::assertSame(
            '2017-06-07',
            (string)$date2,
        );
    }

    public function testToImmutableDateTime(): void
    {
        $moment = Moment::fromStringNotNull('2017-06-07T02:03:04.123456+00:00');
        $dateTimeImmutable = $moment->toImmutableDateTime();

        self::assertSame(
            '2017-06-07T02:03:04.123456+00:00',
            $dateTimeImmutable->format(Moment::FORMAT_MAX_PRECISION),
        );
    }

    public function testToMutableDateTimeWithPositiveDateTime(): void
    {
        $moment = Moment::fromStringNotNull('2017-06-07T02:03:04.123456+00:00');
        $dateTimeMutable = $moment->toMutableDatetime();

        self::assertSame(
            '2017-06-07T02:03:04.123456+00:00',
            $dateTimeMutable->format(Moment::FORMAT_MAX_PRECISION),
        );
    }

    public function testToMutableDateTimeWithNegativeDateTime(): void
    {
        $moment = Moment::fromStringNotNull('-2017-06-07T02:03:04.123456+00:00');
        $dateTimeMutable = $moment->toMutableDatetime();

        self::assertSame(
            '-2017-06-07T02:03:04.123456+00:00',
            $dateTimeMutable->format(Moment::FORMAT_MAX_PRECISION),
        );
    }

    public function testDiffSameTimezone(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06.123456+00:00');
        $moment2 = Moment::fromStringNotNull('2017-02-04T06:08:10.654321+00:00');
        $diff1 = $moment1->diff($moment2);

        self::assertSame(
            'P0Y0M1DT2H3M4S530865F+',
            (string)$diff1,
        );

        $diff2 = $moment2->diff($moment1);

        self::assertSame(
            'P0Y0M1DT2H3M4S530865F-',
            (string)$diff2,
        );
    }

    public function testDiffMixedTimezones(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06.123456+00:00');
        $moment2 = Moment::fromStringNotNull('2017-02-04T10:38:10.654321+04:30');
        $diff1 = $moment1->diff($moment2);

        self::assertSame(
            'P0Y0M1DT2H3M4S530865F+',
            (string)$diff1,
        );

        $diff2 = $moment2->diff($moment1);

        self::assertSame(
            'P0Y0M1DT2H3M4S530865F-',
            (string)$diff2,
        );
    }

    public function testAddWithUtcTimezone(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment2 = $moment1->add(Interval::fromString('P1Y3M2DT6H5M4S'));

        self::assertNotSame(
            $moment1,
            $moment2,
        );

        self::assertSame(
            '2018-05-05T10:10:10.000000+00:00',
            (string)$moment2,
        );
    }

    public function testAddWithOtherTimezone(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06+06:00');
        $moment2 = $moment1->add(Interval::fromString('P1Y3M2DT6H5M4S'));

        self::assertNotSame(
            $moment1,
            $moment2,
        );

        self::assertSame(
            '2018-05-05T10:10:10.000000+06:00',
            (string)$moment2,
        );
    }

    public function testSubWithUtcTimezone(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment2 = $moment1->sub(Interval::fromString('P1Y1M2DT4H5M6S'));

        self::assertNotSame(
            $moment1,
            $moment2,
        );

        self::assertSame(
            '2016-01-01T00:00:00.000000+00:00',
            (string)$moment2,
        );
    }

    public function testSubWithOtherTimezone(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06.123456-06:00');
        $moment2 = $moment1->sub(Interval::fromString('P1Y1M2DT4H5M6S'));

        self::assertNotSame(
            $moment1,
            $moment2,
        );

        self::assertSame(
            '2016-01-01T00:00:00.123456-06:00',
            (string)$moment2,
        );
    }

    public function testIsInTheFuture(): void
    {
        Moment::setNow(Moment::fromStringNotNull('2017-02-03T04:05:06+00:00'));

        $moment1 = Moment::fromStringNotNull('2017-02-03T06:05:05+02:00');
        $moment2 = Moment::fromStringNotNull('2017-02-03T04:05:05+00:00');
        $moment3 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment4 = Moment::fromStringNotNull('2017-02-03T04:05:07+00:00');
        $moment5 = Moment::fromStringNotNull('2017-02-03T04:05:06-01:00');

        self::assertFalse(
            $moment1->isInTheFuture(),
        );

        self::assertFalse(
            $moment2->isInTheFuture(),
        );

        self::assertFalse(
            $moment3->isInTheFuture(),
        );

        self::assertTrue(
            $moment4->isInTheFuture(),
        );

        self::assertTrue(
            $moment5->isInTheFuture(),
        );
    }

    public function testIsInTheFutureOrNow(): void
    {
        Moment::setNow(Moment::fromStringNotNull('2017-02-03T04:05:06+00:00'));

        $moment1 = Moment::fromStringNotNull('2017-02-03T06:05:05+02:00');
        $moment2 = Moment::fromStringNotNull('2017-02-03T04:05:05+00:00');
        $moment3 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment4 = Moment::fromStringNotNull('2017-02-03T04:05:07+00:00');
        $moment5 = Moment::fromStringNotNull('2017-02-03T04:05:06-01:00');

        self::assertFalse(
            $moment1->isInTheFutureOrNow(),
        );

        self::assertFalse(
            $moment2->isInTheFutureOrNow(),
        );

        self::assertTrue(
            $moment3->isInTheFutureOrNow(),
        );

        self::assertTrue(
            $moment4->isInTheFutureOrNow(),
        );

        self::assertTrue(
            $moment5->isInTheFutureOrNow(),
        );
    }

    public function testIsInThePast(): void
    {
        Moment::setNow(Moment::fromStringNotNull('2017-02-03T04:05:06+00:00'));

        $moment1 = Moment::fromStringNotNull('2017-02-03T06:05:05+02:00');
        $moment2 = Moment::fromStringNotNull('2017-02-03T04:05:05+00:00');
        $moment3 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment4 = Moment::fromStringNotNull('2017-02-03T04:05:07+00:00');
        $moment5 = Moment::fromStringNotNull('2017-02-03T04:05:06-01:00');

        self::assertTrue(
            $moment1->isInThePast(),
        );

        self::assertTrue(
            $moment2->isInThePast(),
        );

        self::assertFalse(
            $moment3->isInThePast(),
        );

        self::assertFalse(
            $moment4->isInThePast(),
        );

        self::assertFalse(
            $moment5->isInThePast(),
        );
    }

    public function testIsInThePastOrNow(): void
    {
        Moment::setNow(Moment::fromStringNotNull('2017-02-03T04:05:06+00:00'));

        $moment1 = Moment::fromStringNotNull('2017-02-03T06:05:05+02:00');
        $moment2 = Moment::fromStringNotNull('2017-02-03T04:05:05+00:00');
        $moment3 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment4 = Moment::fromStringNotNull('2017-02-03T04:05:07+00:00');
        $moment5 = Moment::fromStringNotNull('2017-02-03T04:05:06-01:00');

        self::assertTrue(
            $moment1->isInThePastOrNow(),
        );

        self::assertTrue(
            $moment2->isInThePastOrNow(),
        );

        self::assertTrue(
            $moment3->isInThePastOrNow(),
        );

        self::assertFalse(
            $moment4->isInThePastOrNow(),
        );

        self::assertFalse(
            $moment5->isInThePastOrNow(),
        );
    }

    public function testIsAfter(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment2 = Moment::fromStringNotNull('2017-02-03T06:05:05+02:00');
        $moment3 = Moment::fromStringNotNull('2017-02-03T04:05:05+00:00');
        $moment4 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment5 = Moment::fromStringNotNull('2017-02-03T04:05:07+00:00');
        $moment6 = Moment::fromStringNotNull('2017-02-03T04:05:06-01:00');

        self::assertTrue(
            $moment1->isAfter($moment2),
        );

        self::assertTrue(
            $moment1->isAfter($moment3),
        );

        self::assertFalse(
            $moment1->isAfter($moment4),
        );

        self::assertFalse(
            $moment1->isAfter($moment5),
        );

        self::assertFalse(
            $moment1->isAfter($moment6),
        );
    }

    public function testIsAfterOrEqual(): void
    {
        $kMoment1 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $kMoment2 = Moment::fromStringNotNull('2017-02-03T06:05:05+02:00');
        $kMoment3 = Moment::fromStringNotNull('2017-02-03T04:05:05+00:00');
        $kMoment4 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $kMoment5 = Moment::fromStringNotNull('2017-02-03T04:05:07+00:00');
        $kMoment6 = Moment::fromStringNotNull('2017-02-03T04:05:06-01:00');

        self::assertTrue(
            $kMoment1->isAfterOrEqual($kMoment2),
        );

        self::assertTrue(
            $kMoment1->isAfterOrEqual($kMoment3),
        );

        self::assertTrue(
            $kMoment1->isAfterOrEqual($kMoment4),
        );

        self::assertFalse(
            $kMoment1->isAfterOrEqual($kMoment5),
        );

        self::assertFalse(
            $kMoment1->isAfterOrEqual($kMoment6),
        );
    }

    public function testIsBefore(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment2 = Moment::fromStringNotNull('2017-02-03T06:05:05+02:00');
        $moment3 = Moment::fromStringNotNull('2017-02-03T04:05:05+00:00');
        $moment4 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment5 = Moment::fromStringNotNull('2017-02-03T04:05:07+00:00');
        $moment6 = Moment::fromStringNotNull('2017-02-03T04:05:06-01:00');

        self::assertFalse(
            $moment1->isBefore($moment2),
        );

        self::assertFalse(
            $moment1->isBefore($moment3),
        );

        self::assertFalse(
            $moment1->isBefore($moment4),
        );

        self::assertTrue(
            $moment1->isBefore($moment5),
        );

        self::assertTrue(
            $moment1->isBefore($moment6),
        );
    }

    public function testIsBeforeOrEqual(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment2 = Moment::fromStringNotNull('2017-02-03T06:05:05+02:00');
        $moment3 = Moment::fromStringNotNull('2017-02-03T04:05:05+00:00');
        $moment4 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment5 = Moment::fromStringNotNull('2017-02-03T04:05:07+00:00');
        $moment6 = Moment::fromStringNotNull('2017-02-03T04:05:06-01:00');

        self::assertFalse(
            $moment1->isBeforeOrEqual($moment2),
        );

        self::assertFalse(
            $moment1->isBeforeOrEqual($moment3),
        );

        self::assertTrue(
            $moment1->isBeforeOrEqual($moment4),
        );

        self::assertTrue(
            $moment1->isBeforeOrEqual($moment5),
        );

        self::assertTrue(
            $moment1->isBeforeOrEqual($moment6),
        );
    }

    public function testEquals(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment2 = Moment::fromStringNotNull('2017-02-03T06:05:05+02:00');
        $moment3 = Moment::fromStringNotNull('2017-02-03T04:05:05+00:00');
        $moment4 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment5 = Moment::fromStringNotNull('2017-02-03T06:05:06+02:00');
        $moment6 = Moment::fromStringNotNull('2017-02-03T04:05:07+00:00');
        $moment7 = Moment::fromStringNotNull('2017-02-03T04:05:06-01:00');

        self::assertFalse(
            $moment1->equals($moment2),
        );

        self::assertFalse(
            $moment1->equals($moment3),
        );

        self::assertTrue(
            $moment1->equals($moment4),
        );

        self::assertTrue(
            $moment1->equals($moment5),
        );

        self::assertFalse(
            $moment1->equals($moment6),
        );

        self::assertFalse(
            $moment1->equals($moment7),
        );
    }

    public function testGetOffset(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment2 = Moment::fromStringNotNull('2017-02-03T06:05:06+02:00');
        $moment3 = Moment::fromStringNotNull('2017-02-03T00:05:06-04:00');

        self::assertSame(
            0,
            $moment1->getOffset(),
        );

        self::assertSame(
            7200,
            $moment2->getOffset(),
        );

        self::assertSame(
            -14400,
            $moment3->getOffset(),
        );
    }

    public function testGetTimezone(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $timezone1 = $moment1->getTimezone();

        self::assertSame(
            '+00:00',
            $timezone1->getName(),
        );

        $moment2 = Moment::fromStringNotNull('2017-02-03T06:05:06+02:00');
        $timezone2 = $moment2->getTimezone();

        self::assertSame(
            '+02:00',
            $timezone2->getName(),
        );

        $moment3 = Moment::fromStringNotNull('2017-02-03T00:05:06-04:00');
        $timezone3 = $moment3->getTimezone();

        self::assertSame(
            '-04:00',
            $timezone3->getName(),
        );
    }

    public function testWithTimezone(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06+00:00');
        $moment2 = $moment1->withTimezone(new DateTimeZone('+01:00'));

        self::assertNotSame(
            $moment1,
            $moment2,
        );

        self::assertSame(
            '2017-02-03T04:05:06.000000+00:00',
            (string)$moment1,
        );

        self::assertSame(
            '2017-02-03T05:05:06.000000+01:00',
            (string)$moment2,
        );
    }

    public function testWithDate(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06.123456+02:00');
        $moment2 = $moment1->withDate(2016, 3, 4);

        self::assertNotSame(
            $moment1,
            $moment2,
        );

        self::assertSame(
            '2017-02-03T04:05:06.123456+02:00',
            (string)$moment1,
        );

        self::assertSame(
            '2016-03-04T04:05:06.123456+02:00',
            (string)$moment2,
        );
    }

    public function testWithISODate(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06.123456+02:00');
        $moment2 = $moment1->withISODate(2016, 9, 5);

        self::assertNotSame(
            $moment1,
            $moment2,
        );

        self::assertSame(
            '2017-02-03T04:05:06.123456+02:00',
            (string)$moment1,
        );

        self::assertSame(
            '2016-03-04T04:05:06.123456+02:00',
            (string)$moment2,
        );
    }

    public function testWithTime(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06.654321+02:00');
        $moment2 = $moment1->withTime(9, 10, 11, 123456);

        self::assertNotSame(
            $moment1,
            $moment2,
        );

        self::assertSame(
            '2017-02-03T04:05:06.654321+02:00',
            (string)$moment1,
        );

        self::assertSame(
            '2017-02-03T09:10:11.123456+02:00',
            (string)$moment2,
        );
    }

    public function testWithTimestamp(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06+02:00');
        $moment2 = $moment1->withTimestamp((int)gmmktime(5, 8, 9, 5, 6, 2017));

        self::assertNotSame(
            $moment1,
            $moment2,
        );

        self::assertSame(
            '2017-02-03T04:05:06.000000+02:00',
            (string)$moment1,
        );

        self::assertSame(
            '2017-05-06T07:08:09.000000+02:00',
            (string)$moment2,
        );
    }

    public function testModify(): void
    {
        $moment1 = Moment::fromStringNotNull('2017-02-03T04:05:06+02:00');
        $moment2 = $moment1->modify('+1 month');

        self::assertNotSame(
            $moment1,
            $moment2,
        );

        self::assertSame(
            '2017-02-03T04:05:06.000000+02:00',
            (string)$moment1,
        );

        self::assertSame(
            '2017-03-03T04:05:06.000000+02:00',
            (string)$moment2,
        );
    }

    public function testInvalidDatetimeSpecificationThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid datetime specification: InvalidSpecification');

        Moment::fromString('InvalidSpecification');
    }
}
