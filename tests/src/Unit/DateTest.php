<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\DateTime\Unit;

use BjoernGoetschke\DateTime\Date;
use BjoernGoetschke\DateTime\Moment;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class DateTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Moment::resetNow();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Moment::resetNow();
    }

    public function testCreation(): void
    {
        $date1 = Date::fromStringNotNull('2017-02-16');

        self::assertSame(
            2017,
            $date1->getYear(),
        );

        self::assertSame(
            2,
            $date1->getMonth(),
        );

        self::assertSame(
            16,
            $date1->getDay(),
        );

        $date2 = Date::fromStringNotNull('2017-3-4');

        self::assertSame(
            2017,
            $date2->getYear(),
        );

        self::assertSame(
            3,
            $date2->getMonth(),
        );

        self::assertSame(
            4,
            $date2->getDay(),
        );
    }

    public function testInvalidFormatThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);

        Date::fromString('SomeInvalidFormat');
    }

    public function testTimeSetToZero(): void
    {
        $date = Date::fromStringNotNull('2017-01-02T12:13:14.123456+08:00');

        self::assertSame(
            '2017-01-02T00:00:00.000000+00:00',
            $date->format(Moment::FORMAT_MAX_PRECISION),
        );
    }

    public function testToString(): void
    {
        $date = Date::fromStringNotNull('2017-04-05');

        self::assertSame(
            '2017-04-05',
            (string)$date,
        );
    }

    public function testEquals(): void
    {
        $date1 = Date::fromStringNotNull('2017-03-07');
        $date2 = Date::fromStringNotNull('2017-03-07');
        $date3 = Date::fromStringNotNull('2017-03-14');

        self::assertTrue(
            $date1->equals($date2),
        );

        self::assertTrue(
            $date2->equals($date1),
        );

        self::assertFalse(
            $date1->equals($date3),
        );

        self::assertFalse(
            $date2->equals($date3),
        );

        self::assertFalse(
            $date3->equals($date1),
        );

        self::assertFalse(
            $date3->equals($date2),
        );
    }

    public function testToMoment(): void
    {
        $date = Date::fromStringNotNull('2017-04-05');
        $moment = $date->toMoment();

        self::assertSame(
            '2017-04-05T00:00:00.000000+00:00',
            (string)$moment,
        );
    }

    public function testToLocalMoment(): void
    {
        $date = Date::fromStringNotNull('2017-04-05');
        $localMoment = $date->toLocalMoment();

        self::assertSame(
            '2017-04-05T00:00:00.000000',
            (string)$localMoment,
        );
    }

    public function testIsAfter(): void
    {
        $date1 = Date::fromStringNotNull('2017-12-31');
        $date2 = Date::fromStringNotNull('2018-01-01');
        $date3 = Date::fromStringNotNull('2018-01-01');
        $date4 = Date::fromStringNotNull('2018-01-02');

        self::assertFalse(
            $date1->isAfter($date2),
        );

        self::assertFalse(
            $date1->isAfter($date3),
        );

        self::assertFalse(
            $date1->isAfter($date4),
        );

        self::assertTrue(
            $date2->isAfter($date1),
        );

        self::assertFalse(
            $date2->isAfter($date3),
        );

        self::assertFalse(
            $date2->isAfter($date4),
        );

        self::assertTrue(
            $date3->isAfter($date1),
        );

        self::assertFalse(
            $date3->isAfter($date2),
        );

        self::assertFalse(
            $date3->isAfter($date4),
        );

        self::assertTrue(
            $date4->isAfter($date1),
        );

        self::assertTrue(
            $date4->isAfter($date2),
        );

        self::assertTrue(
            $date4->isAfter($date3),
        );
    }

    public function testIsAfterOrEqual(): void
    {
        $date1 = Date::fromStringNotNull('2017-12-31');
        $date2 = Date::fromStringNotNull('2018-01-01');
        $date3 = Date::fromStringNotNull('2018-01-01');
        $date4 = Date::fromStringNotNull('2018-01-02');

        self::assertFalse(
            $date1->isAfterOrEqual($date2),
        );

        self::assertFalse(
            $date1->isAfterOrEqual($date3),
        );

        self::assertFalse(
            $date1->isAfterOrEqual($date4),
        );

        self::assertTrue(
            $date2->isAfterOrEqual($date1),
        );

        self::assertTrue(
            $date2->isAfterOrEqual($date3),
        );

        self::assertFalse(
            $date2->isAfterOrEqual($date4),
        );

        self::assertTrue(
            $date3->isAfterOrEqual($date1),
        );

        self::assertTrue(
            $date3->isAfterOrEqual($date2),
        );

        self::assertFalse(
            $date3->isAfterOrEqual($date4),
        );

        self::assertTrue(
            $date4->isAfterOrEqual($date1),
        );

        self::assertTrue(
            $date4->isAfterOrEqual($date2),
        );

        self::assertTrue(
            $date4->isAfterOrEqual($date3),
        );
    }

    public function testIsBefore(): void
    {
        $date1 = Date::fromStringNotNull('2017-12-31');
        $date2 = Date::fromStringNotNull('2018-01-01');
        $date3 = Date::fromStringNotNull('2018-01-01');
        $date4 = Date::fromStringNotNull('2018-01-02');

        self::assertTrue(
            $date1->isBefore($date2),
        );

        self::assertTrue(
            $date1->isBefore($date3),
        );

        self::assertTrue(
            $date1->isBefore($date4),
        );

        self::assertFalse(
            $date2->isBefore($date1),
        );

        self::assertFalse(
            $date2->isBefore($date3),
        );

        self::assertTrue(
            $date2->isBefore($date4),
        );

        self::assertFalse(
            $date3->isBefore($date1),
        );

        self::assertFalse(
            $date3->isBefore($date2),
        );

        self::assertTrue(
            $date3->isBefore($date4),
        );

        self::assertFalse(
            $date4->isBefore($date1),
        );

        self::assertFalse(
            $date4->isBefore($date2),
        );

        self::assertFalse(
            $date4->isBefore($date3),
        );
    }

    public function testIsBeforeOrEqual(): void
    {
        $date1 = Date::fromStringNotNull('2017-12-31');
        $date2 = Date::fromStringNotNull('2018-01-01');
        $date3 = Date::fromStringNotNull('2018-01-01');
        $date4 = Date::fromStringNotNull('2018-01-02');

        self::assertTrue(
            $date1->isBeforeOrEqual($date2),
        );

        self::assertTrue(
            $date1->isBeforeOrEqual($date3),
        );

        self::assertTrue(
            $date1->isBeforeOrEqual($date4),
        );

        self::assertFalse(
            $date2->isBeforeOrEqual($date1),
        );

        self::assertTrue(
            $date2->isBeforeOrEqual($date3),
        );

        self::assertTrue(
            $date2->isBeforeOrEqual($date4),
        );

        self::assertFalse(
            $date3->isBeforeOrEqual($date1),
        );

        self::assertTrue(
            $date3->isBeforeOrEqual($date2),
        );

        self::assertTrue(
            $date3->isBeforeOrEqual($date4),
        );

        self::assertFalse(
            $date4->isBeforeOrEqual($date1),
        );

        self::assertFalse(
            $date4->isBeforeOrEqual($date2),
        );

        self::assertFalse(
            $date4->isBeforeOrEqual($date3),
        );
    }
}
