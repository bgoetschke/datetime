<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\DateTime\Unit;

use BjoernGoetschke\DateTime\Moment;
use DateTime;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

final class MomentUtilityConstructorsTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Moment::resetNow();
        resetMocks();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Moment::resetNow();
        resetMocks();
    }

    public function testNow(): void
    {
        $moment = Moment::now();

        self::assertNotEmpty(
            (string)$moment,
        );

        self::assertSame(
            '+00:00',
            $moment->format('P'),
        );
    }

    public function testNowReturnsFixedMoment(): void
    {
        Moment::setNow(Moment::fromStringNotNull('2020-01-02T03:04:05.123456+08:00'));

        $moment = Moment::now();

        self::assertSame(
            '2020-01-01T19:04:05.000000+00:00',
            (string)$moment,
        );
    }

    public function testPreciseNow(): void
    {
        $moment = Moment::preciseNow();

        self::assertNotEmpty(
            (string)$moment,
        );

        self::assertSame(
            '+00:00',
            $moment->format('P'),
        );
    }

    public function testPreciseNowReturnsFixedMoment(): void
    {
        Moment::setNow(Moment::fromStringNotNull('2020-01-02T03:04:05.123456+08:00'));

        $moment = Moment::preciseNow();

        self::assertSame(
            '2020-01-01T19:04:05.123456+00:00',
            (string)$moment,
        );
    }

    public function testNowCaching(): void
    {
        global $mock_time;
        $mock_time = function () {
            return gmmktime(10, 26, 40, 10, 31, 2020);
        };

        $moment1 = Moment::now();

        self::assertSame(
            '2020-10-31T10:26:40.000000+00:00',
            (string)$moment1,
        );

        self::assertSame(
            $moment1,
            Moment::now(),
        );

        $mock_time = function () {
            return gmmktime(10, 26, 41, 10, 31, 2020);
        };

        $moment2 = Moment::now();

        self::assertNotSame(
            $moment1,
            $moment2,
        );

        self::assertSame(
            '2020-10-31T10:26:41.000000+00:00',
            (string)$moment2,
        );

        self::assertSame(
            $moment2,
            Moment::now(),
        );
    }

    public function testFromTimestampWithPositiveTimestamp(): void
    {
        $moment = Moment::fromTimestamp((int)gmmktime(10, 26, 40, 10, 31, 2020));

        self::assertSame(
            '2020-10-31T10:26:40.000000+00:00',
            (string)$moment,
        );
    }

    public function testFromTimestampWithNegativeTimestamp(): void
    {
        $moment = Moment::fromTimestamp((int)gmmktime(14, 16, 18, 10, 12, 1492));

        self::assertSame(
            '1492-10-12T14:16:18.000000+00:00',
            (string)$moment,
        );
    }

    public function testFromTimestampOffset(): void
    {
        Moment::setNow(Moment::fromStringNotNull('2017-02-03T06:05:06.123456+02:00'));

        $now = Moment::fromTimestampOffset(0);

        self::assertSame(
            '2017-02-03T04:05:06.000000+00:00',
            (string)$now,
        );

        $inThePast = Moment::fromTimestampOffset(-6 - (60 * 5));

        self::assertSame(
            '2017-02-03T04:00:00.000000+00:00',
            (string)$inThePast,
        );

        $inTheFuture = Moment::fromTimestampOffset(54 + (60 * 54));

        self::assertSame(
            '2017-02-03T05:00:00.000000+00:00',
            (string)$inTheFuture,
        );
    }

    public function testFromString(): void
    {
        $moment = Moment::fromString('2020-06-29T12:00:00.123456+06:00');

        self::assertNotNull(
            $moment,
        );

        self::assertSame(
            '2020-06-29T12:00:00.123456+06:00',
            (string)$moment,
        );
    }

    public function testFromStringWithNull(): void
    {
        $dateTime = null;
        $moment = Moment::fromString($dateTime);

        self::assertNull(
            $moment,
        );
    }

    public function testFromStringIgnoreTz(): void
    {
        $dateTime = '2020-06-29T12:00:00.123456+06:00';
        $moment = Moment::fromStringIgnoreTz($dateTime);

        self::assertNotNull(
            $moment,
        );

        self::assertSame(
            '2020-06-29T12:00:00.123456+00:00',
            (string)$moment,
        );
    }

    public function testFromStringIgnoreTzWithNull(): void
    {
        $dateTime = null;
        $moment = Moment::fromStringIgnoreTz($dateTime);

        self::assertNull(
            $moment,
        );
    }

    public function testFromDateTimeWithPositiveMutable(): void
    {
        $dateTime = new DateTime('2020-06-29T12:00:00.123456+06:00');
        $moment = Moment::fromDateTime($dateTime);

        self::assertNotNull(
            $moment,
        );

        self::assertSame(
            '2020-06-29T12:00:00.123456+06:00',
            (string)$moment,
        );
    }

    public function testFromDateTimeWithNegativeMutable(): void
    {
        $dateTime = new DateTime('-2020-06-29T12:00:00.123456+06:00');
        $moment = Moment::fromDateTime($dateTime);

        self::assertNotNull(
            $moment,
        );

        self::assertSame(
            '-2020-06-29T12:00:00.123456+06:00',
            (string)$moment,
        );
    }

    public function testFromDateTimeWithPositiveImmutable(): void
    {
        $dateTime = new DateTimeImmutable('2020-06-29T12:00:00.123456+06:00');
        $moment = Moment::fromDateTime($dateTime);

        self::assertNotNull(
            $moment,
        );

        self::assertSame(
            '2020-06-29T12:00:00.123456+06:00',
            (string)$moment,
        );
    }

    public function testFromDateTimeWithNegativeImmutable(): void
    {
        $dateTime = new DateTimeImmutable('-2020-06-29T12:00:00.123456+06:00');
        $moment = Moment::fromDateTime($dateTime);

        self::assertNotNull(
            $moment,
        );

        self::assertSame(
            '-2020-06-29T12:00:00.123456+06:00',
            (string)$moment,
        );
    }

    public function testFromDateTimeWithNull(): void
    {
        $dateTime = null;
        $moment = Moment::fromDateTime($dateTime);

        self::assertNull(
            $moment,
        );
    }

    public function testFromDateTimeIgnoreTzWithMutable(): void
    {
        $dateTime = new DateTime('2020-06-29T12:00:00.123456+06:00');
        $moment = Moment::fromDateTimeIgnoreTz($dateTime);

        self::assertSame(
            '2020-06-29T12:00:00.123456+00:00',
            (string)$moment,
        );
    }

    public function testFromDateTimeIgnoreTzWithImmutable(): void
    {
        $dateTime = new DateTimeImmutable('2020-06-29T12:00:00.123456+06:00');
        $moment = Moment::fromDateTimeIgnoreTz($dateTime);

        self::assertSame(
            '2020-06-29T12:00:00.123456+00:00',
            (string)$moment,
        );
    }

    public function testFromDateTimeIgnoreTzWithNull(): void
    {
        $dateTime = null;
        $moment = Moment::fromDateTimeIgnoreTz($dateTime);

        self::assertNull(
            $moment,
        );
    }
}
