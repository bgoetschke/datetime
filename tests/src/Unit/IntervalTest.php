<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\DateTime\Unit;

use BjoernGoetschke\DateTime\Interval;
use BjoernGoetschke\DateTime\Moment;
use DateInterval;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class IntervalTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Moment::resetNow();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Moment::resetNow();
    }

    public function testCreation(): void
    {
        $interval = new Interval(new DateInterval('P2Y5M11DT7H54M33S'));

        self::assertSame(
            2,
            $interval->toDateInterval()->y,
        );

        self::assertSame(
            '2',
            $interval->format('%y'),
        );

        self::assertSame(
            5,
            $interval->toDateInterval()->m,
        );

        self::assertSame(
            '5',
            $interval->format('%m'),
        );

        self::assertSame(
            11,
            $interval->toDateInterval()->d,
        );

        self::assertSame(
            '11',
            $interval->format('%d'),
        );

        self::assertSame(
            7,
            $interval->toDateInterval()->h,
        );

        self::assertSame(
            '7',
            $interval->format('%h'),
        );

        self::assertSame(
            54,
            $interval->toDateInterval()->i,
        );

        self::assertSame(
            '54',
            $interval->format('%i'),
        );

        self::assertSame(
            33,
            $interval->toDateInterval()->s,
        );

        self::assertSame(
            '33',
            $interval->format('%s'),
        );

        self::assertSame(
            0.0,
            $interval->toDateInterval()->f,
        );

        self::assertSame(
            '000000',
            $interval->format('%F'),
        );

        self::assertSame(
            '0',
            $interval->format('%f'),
        );

        self::assertSame(
            0,
            $interval->toDateInterval()->invert,
        );

        self::assertSame(
            '',
            $interval->format('%r'),
        );

        self::assertSame(
            '+',
            $interval->format('%R'),
        );
    }

    public function testFormatAndRecreation(): void
    {
        $interval1 = Interval::fromString('P2Y5M11DT7H54M33S012345F');
        $interval2 = Interval::fromString((string)$interval1);

        self::assertSame(
            2,
            $interval2->toDateInterval()->y,
        );

        self::assertSame(
            '2',
            $interval2->format('%y'),
        );

        self::assertSame(
            5,
            $interval2->toDateInterval()->m,
        );

        self::assertSame(
            '5',
            $interval2->format('%m'),
        );

        self::assertSame(
            11,
            $interval2->toDateInterval()->d,
        );

        self::assertSame(
            '11',
            $interval2->format('%d'),
        );

        self::assertSame(
            7,
            $interval2->toDateInterval()->h,
        );

        self::assertSame(
            '7',
            $interval2->format('%h'),
        );

        self::assertSame(
            54,
            $interval2->toDateInterval()->i,
        );

        self::assertSame(
            '54',
            $interval2->format('%i'),
        );

        self::assertSame(
            33,
            $interval2->toDateInterval()->s,
        );

        self::assertSame(
            '33',
            $interval2->format('%s'),
        );

        self::assertSame(
            0.012345,
            $interval2->toDateInterval()->f,
        );

        self::assertSame(
            '012345',
            $interval2->format('%F'),
        );

        self::assertSame(
            '12345',
            $interval2->format('%f'),
        );

        self::assertSame(
            0,
            $interval2->toDateInterval()->invert,
        );

        self::assertSame(
            '',
            $interval2->format('%r'),
        );

        self::assertSame(
            '+',
            $interval2->format('%R'),
        );
    }

    public function testInvalidIntervalThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid date interval: InvalidInterval');

        Interval::fromString('InvalidInterval');
    }

    public function testToString(): void
    {
        $interval1 = Interval::fromString('P2Y5M11DT7H54M33S012345F');

        self::assertSame(
            'P2Y5M11DT7H54M33S012345F+',
            (string)$interval1,
        );

        $interval2 = Interval::fromString('P2Y5M11DT7H54M33S012345F-');

        self::assertSame(
            'P2Y5M11DT7H54M33S012345F-',
            (string)$interval2,
        );
    }

    public function testWrappedDateIntervalImmutability(): void
    {
        $originalDateInterval = new DateInterval('PT0S');
        $interval = new Interval($originalDateInterval);
        $returnedDateInterval = $interval->toDateInterval();

        self::assertNotSame(
            $originalDateInterval,
            $returnedDateInterval,
        );

        $originalDateInterval->y = 42;

        $returnedDateInterval->h = 13;
        $returnedDateInterval->i = 37;

        self::assertSame(
            'P42Y0M0DT0H0M0S000000F+',
            $originalDateInterval->format('P%yY%mM%dDT%hH%iM%sS%FF%R'),
        );

        self::assertSame(
            'P0Y0M0DT13H37M0S000000F+',
            $returnedDateInterval->format('P%yY%mM%dDT%hH%iM%sS%FF%R'),
        );

        self::assertSame(
            'P0Y0M0DT0H0M0S000000F+',
            $interval->format('P%yY%mM%dDT%hH%iM%sS%FF%R'),
        );
    }
}
