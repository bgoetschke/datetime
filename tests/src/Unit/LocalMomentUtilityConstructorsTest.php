<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\DateTime\Unit;

use BjoernGoetschke\DateTime\LocalMoment;
use BjoernGoetschke\DateTime\Moment;
use DateTime;
use DateTimeImmutable;
use DateTimeZone;
use PHPUnit\Framework\TestCase;

final class LocalMomentUtilityConstructorsTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Moment::resetNow();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Moment::resetNow();
    }

    public function testNow(): void
    {
        Moment::setNow(Moment::fromStringNotNull('2018-01-01T00:00:00.123456+00:00'));

        self::assertSame(
            '2017-12-31T19:00:00.000000',
            (string)LocalMoment::now(new DateTimeZone('-05:00')),
        );

        self::assertSame(
            '2018-01-01T00:00:00.000000',
            (string)LocalMoment::now(new DateTimeZone('+00:00')),
        );

        self::assertSame(
            '2018-01-01T01:00:00.000000',
            (string)LocalMoment::now(new DateTimeZone('+01:00')),
        );
    }

    public function testPreciseNow(): void
    {
        Moment::setNow(Moment::fromStringNotNull('2018-01-01T00:00:00.123456+00:00'));

        self::assertSame(
            '2017-12-31T19:00:00.123456',
            (string)LocalMoment::preciseNow(new DateTimeZone('America/New_York')),
        );

        self::assertSame(
            '2018-01-01T00:00:00.123456',
            (string)LocalMoment::preciseNow(new DateTimeZone('UTC')),
        );

        self::assertSame(
            '2018-01-01T01:00:00.123456',
            (string)LocalMoment::preciseNow(new DateTimeZone('Europe/Berlin')),
        );
    }

    public function testFromStringNullInputReturnsNull(): void
    {
        $moment = LocalMoment::fromString(null);

        self::assertNull(
            $moment,
        );
    }

    public function testFromStringWithoutTimezone(): void
    {
        $moment = LocalMoment::fromString('2020-10-31T11:12:34.123456');

        self::assertNotNull(
            $moment,
        );

        self::assertSame(
            '2020-10-31T11:12:34.123456',
            (string)$moment,
        );

        self::assertSame(
            '2020-10-31T11:12:34.123456+00:00',
            (string)$moment->toMoment(),
        );
    }

    public function testFromStringFullDateTimeAndDifferentDateInUTC(): void
    {
        $moment = LocalMoment::fromString('2020-10-31T23:24:25.123456-08:00');

        self::assertNotNull(
            $moment,
        );

        self::assertSame(
            '2020-10-31T23:24:25.123456',
            (string)$moment,
        );

        self::assertSame(
            '2020-10-31T23:24:25.123456+00:00',
            (string)$moment->toMoment(),
        );
    }

    public function testFromDateTimeNullInputReturnsNull(): void
    {
        $moment = LocalMoment::fromDateTime(null);

        self::assertNull(
            $moment,
        );
    }

    public function testFromDateTimeWithoutTimezone(): void
    {
        $moment1 = LocalMoment::fromDateTime(new DateTimeImmutable('2020-10-31T11:12:34.123456'));

        self::assertNotNull(
            $moment1,
        );

        self::assertSame(
            '2020-10-31T11:12:34.123456',
            (string)$moment1,
        );

        self::assertSame(
            '2020-10-31T11:12:34.123456+00:00',
            (string)$moment1->toMoment(),
        );

        $moment2 = LocalMoment::fromDateTime(new DateTime('2020-10-31T11:12:34.123456'));

        self::assertNotNull(
            $moment2,
        );

        self::assertSame(
            '2020-10-31T11:12:34.123456',
            (string)$moment2,
        );

        self::assertSame(
            '2020-10-31T11:12:34.123456+00:00',
            (string)$moment2->toMoment(),
        );
    }

    public function testFromDateTimeFullDateTimeAndDifferentDateInUTC(): void
    {
        $moment1 = LocalMoment::fromDateTime(new DateTimeImmutable('2020-10-31T23:24:25.123456-08:00'));

        self::assertNotNull(
            $moment1,
        );

        self::assertSame(
            '2020-10-31T23:24:25.123456',
            (string)$moment1,
        );

        self::assertSame(
            '2020-10-31T23:24:25.123456+00:00',
            (string)$moment1->toMoment(),
        );

        $moment2 = LocalMoment::fromDateTime(new DateTime('2020-10-31T23:24:25.123456-08:00'));

        self::assertNotNull(
            $moment2,
        );

        self::assertSame(
            '2020-10-31T23:24:25.123456',
            (string)$moment2,
        );

        self::assertSame(
            '2020-10-31T23:24:25.123456+00:00',
            (string)$moment2->toMoment(),
        );
    }
}
