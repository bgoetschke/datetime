<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\DateTime\Unit;

use BjoernGoetschke\DateTime\Interval;
use BjoernGoetschke\DateTime\Moment;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class IntervalUtilityConstructorsTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Moment::resetNow();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Moment::resetNow();
    }

    public function testFromStringWithFullMicroseconds(): void
    {
        $interval = Interval::fromString('P2Y5M11DT7H54M33S012345F');

        self::assertSame(
            2,
            $interval->toDateInterval()->y,
        );

        self::assertSame(
            '2',
            $interval->format('%y'),
        );

        self::assertSame(
            5,
            $interval->toDateInterval()->m,
        );

        self::assertSame(
            '5',
            $interval->format('%m'),
        );

        self::assertSame(
            11,
            $interval->toDateInterval()->d,
        );

        self::assertSame(
            '11',
            $interval->format('%d'),
        );

        self::assertSame(
            7,
            $interval->toDateInterval()->h,
        );

        self::assertSame(
            '7',
            $interval->format('%h'),
        );

        self::assertSame(
            54,
            $interval->toDateInterval()->i,
        );

        self::assertSame(
            '54',
            $interval->format('%i'),
        );

        self::assertSame(
            33,
            $interval->toDateInterval()->s,
        );

        self::assertSame(
            '33',
            $interval->format('%s'),
        );

        self::assertSame(
            0.012345,
            $interval->toDateInterval()->f,
        );

        self::assertSame(
            '012345',
            $interval->format('%F'),
        );

        self::assertSame(
            '12345',
            $interval->format('%f'),
        );

        self::assertSame(
            0,
            $interval->toDateInterval()->invert,
        );

        self::assertSame(
            '',
            $interval->format('%r'),
        );

        self::assertSame(
            '+',
            $interval->format('%R'),
        );
    }

    public function testFromStringWithExtraLongMicroseconds(): void
    {
        $interval = Interval::fromString('P2Y5M11DT7H54M33S012345678F');

        self::assertSame(
            2,
            $interval->toDateInterval()->y,
        );

        self::assertSame(
            '2',
            $interval->format('%y'),
        );

        self::assertSame(
            5,
            $interval->toDateInterval()->m,
        );

        self::assertSame(
            '5',
            $interval->format('%m'),
        );

        self::assertSame(
            11,
            $interval->toDateInterval()->d,
        );

        self::assertSame(
            '11',
            $interval->format('%d'),
        );

        self::assertSame(
            7,
            $interval->toDateInterval()->h,
        );

        self::assertSame(
            '7',
            $interval->format('%h'),
        );

        self::assertSame(
            54,
            $interval->toDateInterval()->i,
        );

        self::assertSame(
            '54',
            $interval->format('%i'),
        );

        self::assertSame(
            33,
            $interval->toDateInterval()->s,
        );

        self::assertSame(
            '33',
            $interval->format('%s'),
        );

        self::assertSame(
            0.012345,
            $interval->toDateInterval()->f,
        );

        self::assertSame(
            '012345',
            $interval->format('%F'),
        );

        self::assertSame(
            '12345',
            $interval->format('%f'),
        );

        self::assertSame(
            0,
            $interval->toDateInterval()->invert,
        );

        self::assertSame(
            '',
            $interval->format('%r'),
        );

        self::assertSame(
            '+',
            $interval->format('%R'),
        );
    }

    public function testFromStringWithShortMicroseconds(): void
    {
        $interval = Interval::fromString('P2Y5M11DT7H54M33S123f');

        self::assertSame(
            2,
            $interval->toDateInterval()->y,
        );

        self::assertSame(
            '2',
            $interval->format('%y'),
        );

        self::assertSame(
            5,
            $interval->toDateInterval()->m,
        );

        self::assertSame(
            '5',
            $interval->format('%m'),
        );

        self::assertSame(
            11,
            $interval->toDateInterval()->d,
        );

        self::assertSame(
            '11',
            $interval->format('%d'),
        );

        self::assertSame(
            7,
            $interval->toDateInterval()->h,
        );

        self::assertSame(
            '7',
            $interval->format('%h'),
        );

        self::assertSame(
            54,
            $interval->toDateInterval()->i,
        );

        self::assertSame(
            '54',
            $interval->format('%i'),
        );

        self::assertSame(
            33,
            $interval->toDateInterval()->s,
        );

        self::assertSame(
            '33',
            $interval->format('%s'),
        );

        self::assertSame(
            0.000123,
            $interval->toDateInterval()->f,
        );

        self::assertSame(
            '000123',
            $interval->format('%F'),
        );

        self::assertSame(
            '123',
            $interval->format('%f'),
        );

        self::assertSame(
            0,
            $interval->toDateInterval()->invert,
        );

        self::assertSame(
            '',
            $interval->format('%r'),
        );

        self::assertSame(
            '+',
            $interval->format('%R'),
        );
    }

    public function testFromStringWithTooShortMicrosecondsThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);

        Interval::fromString('P2Y5M11DT7H54M33S123F');
    }

    public function testFromStringWithOnlyMicroseconds(): void
    {
        $interval = Interval::fromString('PT123456F+');

        self::assertSame(
            0,
            $interval->toDateInterval()->y,
        );

        self::assertSame(
            '0',
            $interval->format('%y'),
        );

        self::assertSame(
            0,
            $interval->toDateInterval()->m,
        );

        self::assertSame(
            '0',
            $interval->format('%m'),
        );

        self::assertSame(
            0,
            $interval->toDateInterval()->d,
        );

        self::assertSame(
            '0',
            $interval->format('%d'),
        );

        self::assertSame(
            0,
            $interval->toDateInterval()->h,
        );

        self::assertSame(
            '0',
            $interval->format('%h'),
        );

        self::assertSame(
            0,
            $interval->toDateInterval()->i,
        );

        self::assertSame(
            '0',
            $interval->format('%i'),
        );

        self::assertSame(
            0,
            $interval->toDateInterval()->s,
        );

        self::assertSame(
            '0',
            $interval->format('%s'),
        );

        self::assertSame(
            0.123456,
            $interval->toDateInterval()->f,
        );

        self::assertSame(
            '123456',
            $interval->format('%F'),
        );

        self::assertSame(
            '123456',
            $interval->format('%f'),
        );

        self::assertSame(
            0,
            $interval->toDateInterval()->invert,
        );

        self::assertSame(
            '',
            $interval->format('%r'),
        );

        self::assertSame(
            '+',
            $interval->format('%R'),
        );
    }

    public function testPositiveSeconds(): void
    {
        $interval = Interval::seconds(10);
        $moment = Moment::fromStringNotNull('2020-06-29T12:00:00.123456+02:00');

        self::assertSame(
            '2020-06-29T12:00:10.123456+02:00',
            (string)$moment->add($interval),
        );
    }

    public function testNegativeSeconds(): void
    {
        $interval = Interval::seconds(-10);
        $moment = Moment::fromStringNotNull('2020-06-29T12:00:00.123456+02:00');

        self::assertSame(
            '2020-06-29T11:59:50.123456+02:00',
            (string)$moment->add($interval),
        );
    }

    public function testPositiveMinutes(): void
    {
        $interval = Interval::minutes(10);
        $moment = Moment::fromStringNotNull('2020-06-29T12:00:00.123456+02:00');

        self::assertSame(
            '2020-06-29T12:10:00.123456+02:00',
            (string)$moment->add($interval),
        );
    }

    public function testNegativeMinutes(): void
    {
        $interval = Interval::minutes(-10);
        $moment = Moment::fromStringNotNull('2020-06-29T12:00:00.123456+02:00');

        self::assertSame(
            '2020-06-29T11:50:00.123456+02:00',
            (string)$moment->add($interval),
        );
    }

    public function testPositiveHours(): void
    {
        $interval = Interval::hours(10);
        $moment = Moment::fromStringNotNull('2020-06-29T12:00:00.123456+02:00');

        self::assertSame(
            '2020-06-29T22:00:00.123456+02:00',
            (string)$moment->add($interval),
        );
    }

    public function testNegativeHours(): void
    {
        $interval = Interval::hours(-10);
        $moment = Moment::fromStringNotNull('2020-06-29T12:00:00.123456+02:00');

        self::assertSame(
            '2020-06-29T02:00:00.123456+02:00',
            (string)$moment->add($interval),
        );
    }

    public function testPositiveDays(): void
    {
        $interval = Interval::days(10);
        $moment = Moment::fromStringNotNull('2020-06-29T12:00:00.123456+02:00');

        self::assertSame(
            '2020-07-09T12:00:00.123456+02:00',
            (string)$moment->add($interval),
        );
    }

    public function testNegativeDays(): void
    {
        $interval = Interval::days(-10);
        $moment = Moment::fromStringNotNull('2020-06-29T12:00:00.123456+02:00');

        self::assertSame(
            '2020-06-19T12:00:00.123456+02:00',
            (string)$moment->add($interval),
        );
    }

    public function testPositiveMonths(): void
    {
        $interval = Interval::months(10);
        $moment = Moment::fromStringNotNull('2020-06-29T12:00:00.123456+02:00');

        self::assertSame(
            '2021-04-29T12:00:00.123456+02:00',
            (string)$moment->add($interval),
        );
    }

    public function testNegativeMonths(): void
    {
        $interval = Interval::months(-10);
        $moment = Moment::fromStringNotNull('2020-06-29T12:00:00.123456+02:00');

        self::assertSame(
            '2019-08-29T12:00:00.123456+02:00',
            (string)$moment->add($interval),
        );
    }

    public function testPositiveYears(): void
    {
        $interval = Interval::years(10);
        $moment = Moment::fromStringNotNull('2020-06-29T12:00:00.123456+02:00');

        self::assertSame(
            '2030-06-29T12:00:00.123456+02:00',
            (string)$moment->add($interval),
        );
    }

    public function testNegativeYears(): void
    {
        $interval = Interval::years(-10);
        $moment = Moment::fromStringNotNull('2020-06-29T12:00:00.123456+02:00');

        self::assertSame(
            '2010-06-29T12:00:00.123456+02:00',
            (string)$moment->add($interval),
        );
    }
}
