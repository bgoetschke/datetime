<?php

declare(strict_types=1);

namespace {

    global $mock_time;
    $mock_time = null;
    function mock_time(): int
    {
        global $mock_time;
        $function = $mock_time;
        if (!($function instanceof Closure)) {
            $function = Closure::fromCallable('\time');
        }
        return $function();
    }

    function resetMocks(): void
    {
        global $mock_time;
        $mock_time = null;
    }
}

namespace BjoernGoetschke\DateTime {

    function time(): int
    {
        return \mock_time();
    }
}
