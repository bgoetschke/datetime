<?php

declare(strict_types=1);

namespace BjoernGoetschke\DateTime;

use DateTimeInterface;
use DateTimeZone;
use InvalidArgumentException;

/**
 * Represents a moment in time without the timezone.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class LocalMoment
{
    private Moment $moment;

    /**
     * Constructor.
     *
     * @param Moment $moment
     *        The moment that the object will represent.
     * @no-named-arguments
     */
    public function __construct(Moment $moment)
    {
        $this->moment = $moment->discardTimezone();
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Returns the current moment using second precision in the specified timezone.
     *
     * The method uses the {@see time()} function to get the current time, so the fractions of the
     * second will always be 000000.
     *
     * For maximum precision the method {@see LocalMoment::preciseNow()} can be used.
     *
     * @param DateTimeZone $timezone
     *        The timezone to get the current moment for.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     * @see Moment::now()
     */
    public static function now(DateTimeZone $timezone): self
    {
        return new self(Moment::now()->withTimezone($timezone));
    }

    /**
     * Returns the current moment using maximum precision in the specified timezone.
     *
     * For second precision moments the method {@see LocalMoment::now()} can be used.
     *
     * @param DateTimeZone $timezone
     *        The timezone to get the current moment for.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 2.0
     * @see Moment::preciseNow()
     */
    public static function preciseNow(DateTimeZone $timezone): self
    {
        return new self(Moment::preciseNow()->withTimezone($timezone));
    }

    /**
     * Return a moment instance that represents the moment of the specified string.
     *
     * In case the specified moment is null, this method will return null.
     *
     * @param string|null $dateTime
     *        The moment the returned object will represent.
     * @return self|null
     * @throws InvalidArgumentException
     * @no-named-arguments
     * @api usage
     * @since 2.0
     * @see Moment::fromString()
     */
    public static function fromString(?string $dateTime): ?self
    {
        return $dateTime !== null ?
            self::fromStringNotNull($dateTime) :
            null;
    }

    /**
     * Return a moment instance that represents the moment of the specified string.
     *
     * @param string $dateTime
     *        The moment the returned object will represent.
     * @return self
     * @throws InvalidArgumentException
     * @no-named-arguments
     * @api usage
     * @since 2.1
     * @see Moment::fromStringNotNull()
     */
    public static function fromStringNotNull(string $dateTime): self
    {
        return new self(Moment::fromStringNotNull($dateTime));
    }

    /**
     * Return a moment instance that represents the moment of the specified {@see DateTimeInterface} instance.
     *
     * In case the specified moment is null, this method will return null.
     *
     * @param DateTimeInterface|null $dateTime
     *        The moment the returned object will represent.
     * @return self|null
     * @no-named-arguments
     * @api usage
     * @since 2.0
     * @see Moment::fromDateTime()
     */
    public static function fromDateTime(?DateTimeInterface $dateTime): ?self
    {
        return $dateTime !== null ?
            self::fromDateTimeNotNull($dateTime) :
            null;
    }

    /**
     * Return a moment instance that represents the moment of the specified {@see DateTimeInterface} instance.
     *
     * @param DateTimeInterface $dateTime
     *        The moment the returned object will represent.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 2.1
     * @see Moment::fromDateTimeNotNull()
     */
    public static function fromDateTimeNotNull(DateTimeInterface $dateTime): self
    {
        return new self(Moment::fromDateTimeNotNull($dateTime));
    }

    /**
     * Returns the string representation of the moment using the specified format.
     *
     * @param string $format
     *        The format that should be used.
     * @return string
     * @no-named-arguments
     * @api usage
     * @since 1.0
     * @see Moment::format()
     */
    public function format(string $format): string
    {
        return $this->moment->format($format);
    }

    /**
     * Returns a {@see Moment} object with the date and time of this object and timezone set to UTC.
     *
     * @return Moment
     * @api usage
     * @since 1.0
     */
    public function toMoment(): Moment
    {
        return $this->moment;
    }

    /**
     * Returns a {@see Date} object with the date of this object, but time removed.
     *
     * @return Date
     * @api usage
     * @since 1.0
     */
    public function toDate(): Date
    {
        return new Date($this->moment);
    }

    /**
     * Add the specified interval to the moment, returns a new moment object.
     *
     * @param Interval $interval
     *        The interval that should be added to the moment.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function add(Interval $interval): self
    {
        return new self($this->moment->add($interval));
    }

    /**
     * Subtract the specified interval from the moment, returns a new moment object.
     *
     * @param Interval $interval
     *        The interval that should be subtracted from the moment.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function sub(Interval $interval): self
    {
        return new self($this->moment->sub($interval));
    }

    /**
     * Returns an interval that represents the difference between this moment and the specified moment.
     *
     * @param self $otherMoment
     *        The moment that this moment should be compared to.
     * @param bool $absolute
     *        Return an interval that does not indicate whether it is positive or negative.
     * @return Interval
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function diff(self $otherMoment, bool $absolute = false): Interval
    {
        return $this->moment->diff($otherMoment->moment, $absolute);
    }

    /**
     * @return string
     * @since 1.0
     */
    public function __toString(): string
    {
        return $this->format('Y-m-d\TH:i:s.u');
    }

    /**
     * Returns true if the moment is after the specified moment, otherwise false.
     *
     * @param self $otherMoment
     *        The moment that this moment should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function isAfter(self $otherMoment): bool
    {
        return $this->moment->isAfter($otherMoment->moment);
    }

    /**
     * Returns true if the moment is after or equal to the specified moment, otherwise false.
     *
     * @param self $otherMoment
     *        The moment that this moment should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function isAfterOrEqual(self $otherMoment): bool
    {
        return $this->moment->isAfterOrEqual($otherMoment->moment);
    }

    /**
     * Returns true if the moment is before the specified moment, otherwise false.
     *
     * @param self $otherMoment
     *        The moment that this moment should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function isBefore(self $otherMoment): bool
    {
        return $this->moment->isBefore($otherMoment->moment);
    }

    /**
     * Returns true if the moment is before or equal to the specified moment, otherwise false.
     *
     * @param self $otherMoment
     *        The moment that this moment should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function isBeforeOrEqual(self $otherMoment): bool
    {
        return $this->moment->isBeforeOrEqual($otherMoment->moment);
    }

    /**
     * Returns true if the moment is equal to the specified moment, otherwise false.
     *
     * @param self $otherMoment
     *        The moment that this moment should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function equals(self $otherMoment): bool
    {
        return $this->moment->equals($otherMoment->moment);
    }
}
