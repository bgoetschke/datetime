<?php

declare(strict_types=1);

namespace BjoernGoetschke\DateTime;

use DateTimeInterface;
use DateTimeZone;
use InvalidArgumentException;

/**
 * Represents a specific date without actual time or timezone information.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class Date
{
    private Moment $moment;

    /**
     * Constructor.
     *
     * @param Moment $moment
     *        The date that the object will represent.
     * @no-named-arguments
     */
    public function __construct(Moment $moment)
    {
        $this->moment = $moment->discardTimezone()->withTime(0, 0, 0, 0);
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Returns the current date in the specified timezone.
     *
     * @param DateTimeZone $timezone
     *        The timezone to get the current date for.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     * @see Moment::now()
     */
    public static function today(DateTimeZone $timezone): self
    {
        return new self(Moment::now()->withTimezone($timezone));
    }

    /**
     * Return a date instance that represents the date of the specified string.
     *
     * In case the specified date is null, this method will return null.
     *
     * @param string|null $dateTime
     *        The date the returned object will represent.
     * @return self|null
     * @throws InvalidArgumentException
     * @no-named-arguments
     * @api usage
     * @since 2.0
     * @see Moment::fromString()
     */
    public static function fromString(?string $dateTime): ?self
    {
        return $dateTime !== null ?
            self::fromStringNotNull($dateTime) :
            null;
    }

    /**
     * Return a date instance that represents the date of the specified string.
     *
     * @param string $dateTime
     *        The date the returned object will represent.
     * @return self
     * @throws InvalidArgumentException
     * @no-named-arguments
     * @api usage
     * @since 2.1
     * @see Moment::fromStringNotNull()
     */
    public static function fromStringNotNull(string $dateTime): self
    {
        return new self(Moment::fromStringNotNull($dateTime));
    }

    /**
     * Return a date instance that represents the date of the specified {@see DateTimeInterface} instance.
     *
     * In case the specified moment is null, this method will return null.
     *
     * @param DateTimeInterface|null $dateTime
     *        The date the returned object will represent.
     * @return self|null
     * @no-named-arguments
     * @api usage
     * @since 2.0
     * @see Moment::fromDateTime()
     */
    public static function fromDateTime(?DateTimeInterface $dateTime): ?self
    {
        return $dateTime !== null ?
            self::fromDateTimeNotNull($dateTime) :
            null;
    }

    /**
     * Return a date instance that represents the date of the specified {@see DateTimeInterface} instance.
     *
     * @param DateTimeInterface $dateTime
     *        The date the returned object will represent.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 2.1
     * @see Moment::fromDateTimeNotNull()
     */
    public static function fromDateTimeNotNull(DateTimeInterface $dateTime): self
    {
        return new self(Moment::fromDateTimeNotNull($dateTime));
    }

    /**
     * Returns a {@see Moment} object with the date of this object, the time set to 00:00:00 and
     * the timezone set to UTC.
     *
     * @return Moment
     * @api usage
     * @since 1.0
     */
    public function toMoment(): Moment
    {
        return $this->moment;
    }

    /**
     * Returns a {@see LocalMoment} object with the date of this object and time set to 00:00:00.
     *
     * @return LocalMoment
     * @api usage
     * @since 1.0
     */
    public function toLocalMoment(): LocalMoment
    {
        return new LocalMoment($this->moment);
    }

    /**
     * Returns the year of the date.
     *
     * @return int
     * @api usage
     * @since 1.0
     */
    public function getYear(): int
    {
        return (int)$this->moment->format('Y');
    }

    /**
     * Returns the month of the date.
     *
     * @return int
     * @api usage
     * @since 1.0
     */
    public function getMonth(): int
    {
        return (int)$this->moment->format('m');
    }

    /**
     * Returns the day of the date.
     *
     * @return int
     * @api usage
     * @since 1.0
     */
    public function getDay(): int
    {
        return (int)$this->moment->format('d');
    }

    /**
     * Returns the string representation of the date using the specified format.
     *
     * @param string $format
     *        The format that should be used.
     * @return string
     * @no-named-arguments
     * @api usage
     * @since 1.0
     * @see Moment::format()
     */
    public function format(string $format): string
    {
        return $this->moment->format($format);
    }

    /**
     * Returns true if the date is after the specified date, otherwise false.
     *
     * @param self $otherDate
     *        The date that this date should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function isAfter(self $otherDate): bool
    {
        return $this->moment->isAfter($otherDate->moment);
    }

    /**
     * Returns true if the date is after or equal to the specified date, otherwise false.
     *
     * @param self $otherDate
     *        The date that this date should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function isAfterOrEqual(self $otherDate): bool
    {
        return $this->moment->isAfterOrEqual($otherDate->moment);
    }

    /**
     * Returns true if the date is before the specified date, otherwise false.
     *
     * @param self $otherDate
     *        The date that this date should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function isBefore(self $otherDate): bool
    {
        return $this->moment->isBefore($otherDate->moment);
    }

    /**
     * Returns true if the date is before or equal to the specified date, otherwise false.
     *
     * @param self $otherDate
     *        The date that this date should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function isBeforeOrEqual(self $otherDate): bool
    {
        return $this->moment->isBeforeOrEqual($otherDate->moment);
    }

    /**
     * Returns true if the date is equal to the specified date, otherwise false.
     *
     * @param self $otherDate
     *        The date that this date should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function equals(self $otherDate): bool
    {
        return $this->moment->equals($otherDate->moment);
    }

    /**
     * @return string
     * @since 1.0
     */
    public function __toString(): string
    {
        return $this->format('Y-m-d');
    }
}
