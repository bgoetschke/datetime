<?php

declare(strict_types=1);

namespace BjoernGoetschke\DateTime;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use InvalidArgumentException;
use Throwable;

/**
 * Represents a moment in time including the timezone.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class Moment
{
    /**
     * Format that outputs the moment at the maximum supported precision.
     *
     * IMPORTANT: Used by the library internally for conversion of objects where necessary,
     * feel free to use it to convert a moment to a string representation, but it MAY CHANGE over time.
     *
     * @api usage
     * @since 2.0
     * @var string
     */
    public const FORMAT_MAX_PRECISION = 'Y-m-d\TH:i:s.uP';

    private static ?self $fixedNow = null;

    private static ?self $fixedPreciseNow = null;

    private static ?self $cachedNow = null;

    private DateTimeImmutable $dateTime;

    /**
     * Constructor.
     *
     * @param DateTimeInterface $dateTime
     *        The moment that the object will represent.
     * @no-named-arguments
     */
    public function __construct(DateTimeInterface $dateTime)
    {
        if ($dateTime instanceof DateTimeImmutable) {
            $this->dateTime = $dateTime;
        } else {
            /*
             * Explicitly use new DateTimeImmutable() instead of DateTimeImmutable::createFromFormat() because
             * the latter cannot handle dates with a negative year number.
             *
             * See https://bugs.php.net/bug.php?id=76785
             */
            $this->dateTime = new DateTimeImmutable(
                $dateTime->format('Y-m-d\TH:i:s.u'),
                $dateTime->getTimezone(),
            );
        }
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Set the moment returned by {@see Moment::now()} and {@see Moment::preciseNow()}, the timezone
     * will be converted to UTC.
     *
     * IMPORTANT: Setting a fixed moment is intended to be used during UNIT TESTS, NOT in actual business logic!
     *
     * @param self $moment
     *        The moment that should be the fixed moment.
     * @no-named-arguments
     * @api usage
     * @since 1.0
     * @see Moment::now()
     * @see Moment::preciseNow()
     * @see Moment::hasNow()
     * @see Moment::resetNow()
     */
    public static function setNow(self $moment): void
    {
        self::$fixedPreciseNow = $moment->withUtcTimezone();
        self::$fixedNow = self::$fixedPreciseNow->withTime(
            (int)self::$fixedPreciseNow->format('H'),
            (int)self::$fixedPreciseNow->format('i'),
            (int)self::$fixedPreciseNow->format('s'),
        );
        self::$cachedNow = null;
    }

    /**
     * Returns true if the moment returned by {@see Moment::now()} and {@see Moment::preciseNow()} is fixed,
     * otherwise false.
     *
     * IMPORTANT: Setting a fixed moment is intended to be used during UNIT TESTS, NOT in actual business logic!
     *
     * @api usage
     * @since 3.0
     * @see Moment::now()
     * @see Moment::preciseNow()
     * @see Moment::setNow()
     * @see Moment::resetNow()
     */
    public static function hasNow(): bool
    {
        return self::$fixedPreciseNow !== null || self::$fixedNow !== null;
    }

    /**
     * Reset the fixed moment returned by {@see Moment::now()} and {@see Moment::preciseNow()}.
     *
     * IMPORTANT: Setting a fixed moment is intended to be used during UNIT TESTS, NOT in actual business logic!
     *
     * @api usage
     * @since 1.0
     * @see Moment::now()
     * @see Moment::preciseNow()
     * @see Moment::setNow()
     * @see Moment::hasNow()
     */
    public static function resetNow(): void
    {
        self::$fixedPreciseNow = null;
        self::$fixedNow = null;
        self::$cachedNow = null;
    }

    /**
     * Returns the current moment using second precision with UTC timezone.
     *
     * The method uses the {@see time()} function to get the current time, so the fractions of the
     * second will always be 000000.
     *
     * For maximum precision the method {@see Moment::preciseNow()} can be used.
     *
     * @return self
     * @api usage
     * @since 1.0
     * @see Moment::preciseNow()
     * @see Moment::setNow()
     * @see Moment::hasNow()
     * @see Moment::resetNow()
     */
    public static function now(): self
    {
        if (self::$fixedNow !== null) {
            return self::$fixedNow;
        }

        $time = time();
        if (self::$cachedNow === null || self::$cachedNow->getTimestamp() !== $time) {
            self::$cachedNow = self::fromTimestamp($time);
        }

        return self::$cachedNow;
    }

    /**
     * Returns the current moment using maximum precision with UTC timezone.
     *
     * For second precision moments the method {@see Moment::now()} can be used.
     *
     * @return self
     * @api usage
     * @since 2.0
     * @see Moment::now()
     * @see Moment::setNow()
     * @see Moment::hasNow()
     * @see Moment::resetNow()
     */
    public static function preciseNow(): self
    {
        if (self::$fixedPreciseNow !== null) {
            return self::$fixedPreciseNow;
        }

        return self::fromDateTimeIgnoreTzNotNull(new DateTimeImmutable());
    }

    /**
     * Returns a moment that represents the specified unix timestamp with UTC timezone.
     *
     * Because the method takes the timestamp as integer, the fractions of the
     * second will always be 000000.
     *
     * @param int $unixTimestamp
     *        The unix timestamp the moment should represent.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 2.0
     */
    public static function fromTimestamp(int $unixTimestamp): self
    {
        $dateTime = DateTimeImmutable::createFromFormat(
            'U',
            (string)$unixTimestamp,
            new DateTimeZone('UTC'),
        );
        assert($dateTime !== false);
        return new self($dateTime);
    }

    /**
     * Returns a moment that is $offset seconds in the past/future with UTC timezone.
     *
     * The method uses the {@see Moment::now()} and {@see Moment::getTimestamp()} methods to get the
     * current time, so the fractions of the second will always be 000000.
     *
     * @param int $offset
     *        Number of seconds the moment should offset from the current moment.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 2.0
     * @see Moment::now()
     */
    public static function fromTimestampOffset(int $offset): self
    {
        return self::fromTimestamp(self::now()->getTimestamp() + $offset);
    }

    /**
     * Return a moment instance that represents the moment of the specified string.
     *
     * In case the specified moment is null, this method will return null.
     *
     * The parameters are passed to {@see DateTimeImmutable::__construct()} to create the internal
     * representation of the moment.
     *
     * @param string|null $dateTime
     *        The moment the returned object will represent.
     * @param DateTimeZone|null $timezone
     *        The timezone the returned object will have (this parameter is ignored in case
     *        the $dateTime parameter contains a timezone specification).
     * @return self|null
     * @throws InvalidArgumentException
     * @no-named-arguments
     * @api usage
     * @since 2.0
     * @see DateTimeImmutable::__construct()
     */
    public static function fromString(?string $dateTime, ?DateTimeZone $timezone = null): ?self
    {
        return $dateTime !== null ?
            self::fromStringNotNull($dateTime, $timezone) :
            null;
    }

    /**
     * Return a moment instance that represents the moment of the specified string.
     *
     * The parameters are passed to {@see DateTimeImmutable::__construct()} to create the internal
     * representation of the moment.
     *
     * @param string $dateTime
     *        The moment the returned object will represent.
     * @param DateTimeZone|null $timezone
     *        The timezone the returned object will have (this parameter is ignored in case
     *        the $dateTime parameter contains a timezone specification).
     * @return self
     * @throws InvalidArgumentException
     * @no-named-arguments
     * @api usage
     * @since 2.1
     * @see DateTimeImmutable::__construct()
     */
    public static function fromStringNotNull(string $dateTime, ?DateTimeZone $timezone = null): self
    {
        try {
            $dateTimeInstance = new DateTimeImmutable($dateTime, $timezone);
        } catch (Throwable $e) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid datetime specification: %1$s',
                    $dateTime,
                ),
                0,
                $e,
            );
        }

        return new self($dateTimeInstance);
    }

    /**
     * Return a moment instance that represents the moment of the specified string.
     *
     * The timezone of the specified moment will be ignored and not be converted to UTC, so the returned moment
     * will have the same date and time, but the timezone will be replaced with UTC/+00:00.
     *
     * Example: 2020-06-29T12:00:00+06:00 will be returned as 2020-06-29T12:00:00+00:00
     *
     * In case the specified moment is null, this method will return null.
     *
     * The parameters are passed to {@see DateTimeImmutable::__construct()} to create the internal
     * representation of the moment.
     *
     * @param string|null $dateTime
     *        The moment the returned object will represent.
     * @param DateTimeZone|null $timezone
     *        The timezone the returned object will have (this parameter is ignored in case
     *        the $dateTime parameter contains a timezone specification).
     * @return self|null
     * @throws InvalidArgumentException
     * @no-named-arguments
     * @api usage
     * @since 2.0
     * @see DateTimeImmutable::__construct()
     */
    public static function fromStringIgnoreTz(?string $dateTime, ?DateTimeZone $timezone = null): ?self
    {
        return $dateTime !== null ?
            self::fromStringIgnoreTzNotNull($dateTime, $timezone) :
            null;
    }

    /**
     * Return a moment instance that represents the moment of the specified string.
     *
     * The timezone of the specified moment will be ignored and not be converted to UTC, so the returned moment
     * will have the same date and time, but the timezone will be replaced with UTC/+00:00.
     *
     * Example: 2020-06-29T12:00:00+06:00 will be returned as 2020-06-29T12:00:00+00:00
     *
     * The parameters are passed to {@see DateTimeImmutable::__construct()} to create the internal
     * representation of the moment.
     *
     * @param string $dateTime
     *        The moment the returned object will represent.
     * @param DateTimeZone|null $timezone
     *        The timezone the returned object will have (this parameter is ignored in case
     *        the $dateTime parameter contains a timezone specification).
     * @return self
     * @throws InvalidArgumentException
     * @no-named-arguments
     * @api usage
     * @since 2.1
     * @see DateTimeImmutable::__construct()
     */
    public static function fromStringIgnoreTzNotNull(string $dateTime, ?DateTimeZone $timezone = null): self
    {
        return self::fromStringNotNull($dateTime, $timezone)->discardTimezone();
    }

    /**
     * Return a moment instance that represents the moment of the specified {@see DateTimeInterface} instance.
     *
     * In case the specified moment is null, this method will return null.
     *
     * @param DateTimeInterface|null $dateTime
     *        The moment the returned object will represent.
     * @return self|null
     * @no-named-arguments
     * @api usage
     * @since 1.1
     */
    public static function fromDateTime(?DateTimeInterface $dateTime): ?self
    {
        return $dateTime !== null ?
            self::fromDateTimeNotNull($dateTime) :
            null;
    }

    /**
     * Return a moment instance that represents the moment of the specified {@see DateTimeInterface} instance.
     *
     * @param DateTimeInterface $dateTime
     *        The moment the returned object will represent.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 2.1
     */
    public static function fromDateTimeNotNull(DateTimeInterface $dateTime): self
    {
        return new self($dateTime);
    }

    /**
     * Return a moment instance that represents the moment of the specified {@see DateTimeInterface} instance.
     *
     * The timezone of the specified moment will be ignored and not be converted to UTC, so the returned moment
     * will have the same date and time, but the timezone will be replaced with UTC/+00:00.
     *
     * Example: 2020-06-29T12:00:00+06:00 will be returned as 2020-06-29T12:00:00+00:00
     *
     * In case the specified moment is null, this method will return null.
     *
     * @param DateTimeInterface|null $dateTime
     *        The moment the returned object will represent.
     * @return self|null
     * @no-named-arguments
     * @api usage
     * @since 1.1
     */
    public static function fromDateTimeIgnoreTz(?DateTimeInterface $dateTime): ?self
    {
        return $dateTime !== null ?
            self::fromDateTimeIgnoreTzNotNull($dateTime) :
            null;
    }

    /**
     * Return a moment instance that represents the moment of the specified {@see DateTimeInterface} instance.
     *
     * The timezone of the specified moment will be ignored and not be converted to UTC, so the returned moment
     * will have the same date and time, but the timezone will be replaced with UTC/+00:00.
     *
     * Example: 2020-06-29T12:00:00+06:00 will be returned as 2020-06-29T12:00:00+00:00
     *
     * @param DateTimeInterface $dateTime
     *        The moment the returned object will represent.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 2.1
     */
    public static function fromDateTimeIgnoreTzNotNull(DateTimeInterface $dateTime): self
    {
        return self::fromDateTimeNotNull($dateTime)->discardTimezone();
    }

    private function updateClone(DateTimeImmutable $newDateTime): self
    {
        $moment = clone $this;
        $moment->dateTime = $newDateTime;
        return $moment;
    }

    /**
     * Returns the string representation of the moment using the specified format.
     *
     * @param string $format
     *        The format that should be used.
     * @return string
     * @no-named-arguments
     * @api usage
     * @since 1.0
     * @see DateTimeInterface::format()
     */
    public function format(string $format): string
    {
        return $this->dateTime->format($format);
    }

    /**
     * Set the timezone of the moment to UTC, returns a new moment object.
     *
     * @return self
     * @api usage
     * @since 1.0
     */
    public function withUtcTimezone(): self
    {
        return $this->withTimezone(new DateTimeZone('UTC'));
    }

    /**
     * Set the timezone of the moment to UTC without changing the actual date and time, returns a new moment object.
     *
     * The timezone of the moment will be ignored and not be converted to UTC, so the returned moment
     * will have the same date and time, but the timezone will be replaced with UTC/+00:00.
     *
     * Example: 2020-06-29T12:00:00.123456+06:00 will be returned as 2020-06-29T12:00:00.123456+00:00
     *
     * @return self
     * @api usage
     * @since 2.0
     * @see Moment::replaceTimezone()
     */
    public function discardTimezone(): self
    {
        return $this->replaceTimezone(new DateTimeZone('UTC'));
    }

    /**
     * Set the timezone of the moment to the specified timezone
     * without changing the actual date and time, returns a new moment object.
     *
     * The timezone of the moment will be ignored and not be converted to to the specified timezone,
     * so the returned moment will have the same date and time, but the timezone will be replaced.
     *
     * Example when the timezone "+02:00" is specified:
     * 2020-06-29T12:00:00.123456+06:00 will be returned as 2020-06-29T12:00:00.123456+02:00
     *
     * To change the timezone and convert the date and time of the moment to that timezone,
     * {@see Moment::withTimezone()} can be used.
     *
     * @param DateTimeZone $timezone
     *        The new timezone that should be used.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 2.0
     */
    public function replaceTimezone(DateTimeZone $timezone): self
    {
        return self::fromStringNotNull($this->format('Y-m-d\TH:i:s.u'), $timezone);
    }

    /**
     * Returns true if the moment is in the future, otherwise false.
     *
     * @return bool
     * @api usage
     * @since 1.0
     */
    public function isInTheFuture(): bool
    {
        return $this->isAfter(self::now());
    }

    /**
     * Returns true if the moment is in the future or now, otherwise false.
     *
     * @return bool
     * @api usage
     * @since 1.0
     */
    public function isInTheFutureOrNow(): bool
    {
        return $this->isAfterOrEqual(self::now());
    }

    /**
     * Returns true if the moment is in the past, otherwise false.
     *
     * @return bool
     * @api usage
     * @since 1.0
     */
    public function isInThePast(): bool
    {
        return $this->isBefore(self::now());
    }

    /**
     * Returns true if the moment is in the past or now, otherwise false.
     *
     * @return bool
     * @api usage
     * @since 1.0
     */
    public function isInThePastOrNow(): bool
    {
        return $this->isBeforeOrEqual(self::now());
    }

    /**
     * Returns true if the moment is after the specified moment, otherwise false.
     *
     * @param self $otherMoment
     *        The moment that this moment should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function isAfter(self $otherMoment): bool
    {
        return $this->getTimestamp() > $otherMoment->getTimestamp();
    }

    /**
     * Returns true if the moment is after or equal to the specified moment, otherwise false.
     *
     * @param self $otherMoment
     *        The moment that this moment should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function isAfterOrEqual(self $otherMoment): bool
    {
        return $this->getTimestamp() >= $otherMoment->getTimestamp();
    }

    /**
     * Returns true if the moment is before the specified moment, otherwise false.
     *
     * @param self $otherMoment
     *        The moment that this moment should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function isBefore(self $otherMoment): bool
    {
        return $this->getTimestamp() < $otherMoment->getTimestamp();
    }

    /**
     * Returns true if the moment is before or equal to the specified moment, otherwise false.
     *
     * @param self $otherMoment
     *        The moment that this moment should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function isBeforeOrEqual(self $otherMoment): bool
    {
        return $this->getTimestamp() <= $otherMoment->getTimestamp();
    }

    /**
     * Returns true if the moment is equal to the specified moment, otherwise false.
     *
     * @param self $otherMoment
     *        The moment that this moment should be compared to.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function equals(self $otherMoment): bool
    {
        return $this->getTimestamp() == $otherMoment->getTimestamp();
    }

    /**
     * Add the specified interval to the moment, returns a new moment object.
     *
     * @param Interval $interval
     *        The interval that should be added to the moment.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function add(Interval $interval): self
    {
        return $this->updateClone($this->dateTime->add($interval->toDateInterval()));
    }

    /**
     * Subtract the specified interval from the moment, returns a new moment object.
     *
     * @param Interval $interval
     *        The interval that should be subtracted from the moment.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function sub(Interval $interval): self
    {
        return $this->updateClone($this->dateTime->sub($interval->toDateInterval()));
    }

    /**
     * Returns an interval that represents the difference between this moment and the specified moment.
     *
     * @param self $otherMoment
     *        The moment that this moment should be compared to.
     * @param bool $absolute
     *        Return an interval that does not indicate whether it is positive or negative.
     * @return Interval
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function diff(self $otherMoment, bool $absolute = false): Interval
    {
        // \DateTimeInterface::diff() does not work correctly with anything
        // but \DateTime objects in earlier PHP versions
        return new Interval(
            $this->toMutableDatetime()->diff(
                $otherMoment->toMutableDatetime(),
                $absolute,
            ),
        );
    }

    /**
     * Returns the timezone offset of the moment in seconds.
     *
     * @return int
     * @api usage
     * @since 1.0
     */
    public function getOffset(): int
    {
        return $this->dateTime->getOffset();
    }

    /**
     * Returns the timezone of the moment.
     *
     * @return DateTimeZone
     * @api usage
     * @since 1.0
     */
    public function getTimezone(): DateTimeZone
    {
        return $this->dateTime->getTimezone();
    }

    /**
     * Sets the timezone of the moment to the specified timezone, returns a new moment object.
     *
     * To change the timezone without converting the date and time of the moment that timezone,
     * {@see Moment::replaceTimezone()} can be used.
     *
     * @param DateTimeZone $timezone
     *        The new timezone that should be used.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     * @see DateTimeImmutable::setTimezone()
     */
    public function withTimezone(DateTimeZone $timezone): self
    {
        return $this->updateClone($this->dateTime->setTimezone($timezone));
    }

    /**
     * Sets the date of the moment to the specified date, returns a new moment object.
     *
     * @param int $year
     *        The year that should be set.
     * @param int $month
     *        The month that should be set.
     * @param int $day
     *        The day that should be set.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     * @see DateTimeImmutable::setDate()
     */
    public function withDate(int $year, int $month, int $day): self
    {
        return $this->updateClone($this->dateTime->setDate($year, $month, $day));
    }

    /**
     * Sets the date of the moment to the specified ISO date, returns a new moment object.
     *
     * @param int $year
     *        The year that should be set.
     * @param int $week
     *        The week of the year that should be set.
     * @param int $day
     *        The day of the week that should be set.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     * @see DateTimeImmutable::setISODate()
     */
    public function withISODate(int $year, int $week, int $day = 1): self
    {
        return $this->updateClone($this->dateTime->setISODate($year, $week, $day));
    }

    /**
     * Sets the time of the moment to the specified time, returns a new moment object.
     *
     * @param int $hour
     *        The hour that should be set.
     * @param int $minute
     *        The minute that should be set.
     * @param int $second
     *        The second that should be set.
     * @param int $microseconds
     *        The microseconds that should be set.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     * @see DateTimeImmutable::setTime()
     */
    public function withTime(int $hour, int $minute, int $second = 0, int $microseconds = 0): self
    {
        return $this->updateClone($this->dateTime->setTime($hour, $minute, $second, $microseconds));
    }

    /**
     * Sets moment to the specified unix timestamp, returns a new moment object.
     *
     * @param int $unixTimestamp
     *        The unix timestamp that should be used.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     * @see DateTimeImmutable::setTimestamp()
     */
    public function withTimestamp(int $unixTimestamp): self
    {
        return $this->updateClone($this->dateTime->setTimestamp($unixTimestamp));
    }

    /**
     * Returns the unix timestamp of the moment.
     *
     * @return int
     * @api usage
     * @since 1.0
     */
    public function getTimestamp(): int
    {
        return $this->dateTime->getTimestamp();
    }

    /**
     * Returns a {@see LocalMoment} object with the date and time of this object, but the timezone removed.
     *
     * @return LocalMoment
     * @api usage
     * @since 1.0
     */
    public function toLocalMoment(): LocalMoment
    {
        return new LocalMoment($this);
    }

    /**
     * Returns a {@see Date} object with the date of this object, but time and timezone removed.
     *
     * @return Date
     * @api usage
     * @since 1.0
     */
    public function toDate(): Date
    {
        return new Date($this);
    }

    /**
     * Modifies the moment by the specified modification, returns a new moment object.
     *
     * @param string $modification
     *        The modification that should be used.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     * @see DateTimeImmutable::modify()
     */
    public function modify(string $modification): self
    {
        return $this->updateClone($this->dateTime->modify($modification));
    }

    /**
     * @return string
     * @since 1.0
     */
    public function __toString(): string
    {
        return $this->format(self::FORMAT_MAX_PRECISION);
    }

    /**
     * Returns a {@see DateTimeImmutable} object that represents this moment.
     *
     * @return DateTimeImmutable
     * @api usage
     * @since 1.0
     */
    public function toImmutableDateTime(): DateTimeImmutable
    {
        return $this->dateTime;
    }

    /**
     * Returns a {@see DateTime} object that represents this moment.
     *
     * @return DateTime
     * @api usage
     * @since 1.0
     */
    public function toMutableDatetime(): DateTime
    {
        /*
         * Explicitly use new DateTime() instead of DateTime::createFromFormat() because
         * the latter cannot handle dates with a negative year number.
         *
         * See https://bugs.php.net/bug.php?id=76785
         */
        return new DateTime(
            $this->dateTime->format('Y-m-d\TH:i:s.u'),
            $this->dateTime->getTimezone(),
        );
    }
}
