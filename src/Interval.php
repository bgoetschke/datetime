<?php

declare(strict_types=1);

namespace BjoernGoetschke\DateTime;

use DateInterval;
use InvalidArgumentException;
use Throwable;

/**
 * Represents a time interval.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class Interval
{
    private DateInterval $dateInterval;

    /**
     * Constructor.
     *
     * @param DateInterval $dateInterval
     *        The interval that the object will represent.
     * @no-named-arguments
     */
    public function __construct(DateInterval $dateInterval)
    {
        $this->dateInterval = clone $dateInterval;
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Return an interval that represents the interval of the specified string.
     *
     * @param string $interval
     *        The interval that the object will represent.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 2.0
     */
    public static function fromString(string $interval): self
    {
        $originalInterval = $interval;
        $modifiedTime = false;

        /*
         * DateInterval::__construct() does not support microseconds
         */
        $microseconds = 0.0;
        if ((bool)preg_match('=([0-9]{6,})F=', $interval, $matches)) {
            $interval = str_replace($matches[0], '', $interval);
            $microseconds = (float)('0.' . mb_substr($matches[1], 0, 6));
            $modifiedTime = true;
        } elseif ((bool)preg_match('=([0-9]+)f=', $interval, $matches)) {
            $interval = str_replace($matches[0], '', $interval);
            $microseconds = (float)('0.' . str_pad($matches[1], 6, '0', STR_PAD_LEFT));
            $modifiedTime = true;
        }

        /*
         * DateInterval::__construct() does not support signed intervals
         */
        $invert = 0;
        if (mb_substr($interval, -1, 1) === '+') {
            $interval = mb_substr($interval, 0, -1);
        } elseif (mb_substr($interval, -1, 1) === '-') {
            $interval = mb_substr($interval, 0, -1);
            $invert = 1;
        }

        /*
         * In case we removed stuff from the interval it may be possible that there is no time specification left, add
         * a "0 seconds" specification in this case to have something there for DateInterval::__construct() to parse
         *
         * Example: "PT123456F" (microseconds will be removed) -> "PT" (invalid) -> "PT0S" (valid again)
         */
        if ($modifiedTime && mb_substr($interval, -1, 1) === 'T') {
            $interval .= '0S';
        }

        try {
            $dateInterval = new DateInterval($interval);
            $dateInterval->f = $microseconds;
            $dateInterval->invert = $invert;
        } catch (Throwable $e) {
            throw new InvalidArgumentException(
                sprintf('Invalid date interval: %1$s', $originalInterval),
                0,
                $e,
            );
        }

        return new self($dateInterval);
    }

    /**
     * Create an interval that represents the specified number of seconds.
     *
     * @param int $seconds
     *        The number of seconds the interval will represent, can be negative.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.1
     */
    public static function seconds(int $seconds): self
    {
        $operator = ($seconds < 0) ? '-' : '+';
        return self::fromString('PT' . abs($seconds) . 'S' . $operator);
    }

    /**
     * Create an interval that represents the specified number of minutes.
     *
     * @param int $minutes
     *        The number of minutes the interval will represent, can be negative.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.1
     */
    public static function minutes(int $minutes): self
    {
        $operator = ($minutes < 0) ? '-' : '+';
        return self::fromString('PT' . abs($minutes) . 'M' . $operator);
    }

    /**
     * Create an interval that represents the specified number of hours.
     *
     * @param int $hours
     *        The number of hours the interval will represent, can be negative.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.1
     */
    public static function hours(int $hours): self
    {
        $operator = ($hours < 0) ? '-' : '+';
        return self::fromString('PT' . abs($hours) . 'H' . $operator);
    }

    /**
     * Create an interval that represents the specified number of days.
     *
     * @param int $days
     *        The number of days the interval will represent, can be negative.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.1
     */
    public static function days(int $days): self
    {
        $operator = ($days < 0) ? '-' : '+';
        return self::fromString('P' . abs($days) . 'D' . $operator);
    }

    /**
     * Create an interval that represents the specified number of months.
     *
     * @param int $months
     *        The number of months the interval will represent, can be negative.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.1
     */
    public static function months(int $months): self
    {
        $operator = ($months < 0) ? '-' : '+';
        return self::fromString('P' . abs($months) . 'M' . $operator);
    }

    /**
     * Create an interval that represents the specified number of years.
     *
     * @param int $years
     *        The number of years the interval will represent, can be negative.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.1
     */
    public static function years(int $years): self
    {
        $operator = ($years < 0) ? '-' : '+';
        return self::fromString('P' . abs($years) . 'Y' . $operator);
    }

    /**
     * Returns the string representation of the interval using the specified format.
     *
     * @param string $format
     *        The format that should be used.
     * @return string
     * @no-named-arguments
     * @api usage
     * @since 1.0
     * @see DateInterval::format()
     */
    public function format(string $format): string
    {
        return $this->dateInterval->format($format);
    }

    /**
     * @return string
     * @since 1.1
     */
    public function __toString(): string
    {
        return $this->format('P%yY%mM%dDT%hH%iM%sS%FF%R');
    }

    /**
     * Returns a new copy/clone of the date interval object to prevent accidental modification since php
     * does not have an immutable DateInterval object.
     *
     * @return DateInterval
     * @since 1.0
     * @api usage
     */
    public function toDateInterval(): DateInterval
    {
        return clone $this->dateInterval;
    }
}
